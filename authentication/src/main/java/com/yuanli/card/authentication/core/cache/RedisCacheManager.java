package com.yuanli.card.authentication.core.cache;

import com.yuanli.card.constant.ResponseCode;
import com.yuanli.card.exception.ServiceException;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by layne on 2018/5/24.
 */
public class RedisCacheManager implements ICacheManager {
    Logger logger = LoggerFactory.getLogger(RedisCacheManager.class);
    // fast lookup by name map
    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<>();

    private final RedisTemplate redisTemplate;

    public RedisCacheManager(RedisTemplate redisTemplate) {
        if (redisTemplate == null){
            throw new ServiceException(ResponseCode.SYSTEM_ERROR);
        }
        this.redisTemplate = redisTemplate;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        Cache c = caches.get(name);
        if (c == null) {
            c = new RedisCache<K, V>(redisTemplate, name + ":");
            caches.put(name, c);
        }
        logger.info("获取名称为:{} 的RedisCache实例,total:{}", name, caches.size());
        return c;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name, Long timeout) throws CacheException {
        logger.info("获取名称为: " + name + " 的RedisCache实例,total:" + caches.size());
        Cache c = caches.get(name);
        if (c == null) {
            logger.info("-----------new caches---------");
            c = new RedisCache<K, V>(redisTemplate, name + ":", timeout);
            caches.put(name, c);
        }
        return c;
    }
}
