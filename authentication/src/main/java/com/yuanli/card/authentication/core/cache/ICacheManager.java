package com.yuanli.card.authentication.core.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * Created by layne on 2018/5/24.
 */
public interface ICacheManager extends CacheManager {
    /**
     * 可以指定过期时间的缓存
     *
     * @param name
     * @param timeout 单位 毫秒
     * @param <K>
     * @param <V>
     * @return
     * @throws CacheException
     */
    <K, V> Cache<K, V> getCache(String name, Long timeout) throws CacheException;
}
