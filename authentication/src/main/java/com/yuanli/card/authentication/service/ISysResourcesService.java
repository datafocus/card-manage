package com.yuanli.card.authentication.service;


import com.yuanli.card.pojo.SysResources;

import java.util.List;

/**
 * Created by layne on 2018/5/23.
 */
public interface ISysResourcesService {

    List<SysResources> listUrlAndPermission();

    List<SysResources> listByUserId(Long userId);

    List<SysResources> findAll();

    void addSysRes(SysResources sysResources);
}
