package com.yuanli.card.authentication.service;

import java.util.Map;

/**
 * Created by layne on 2018/5/23.
 */
public interface IShiroService {
    Map<String, String> loadFilterChainDefinitions();
}
