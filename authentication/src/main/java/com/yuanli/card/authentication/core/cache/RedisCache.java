package com.yuanli.card.authentication.core.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by layne on 2018/5/24.
 */
public class RedisCache<K, V> implements Cache<K, V> {

    private final RedisTemplate<String, V> redisTemplate;
    private final String keyPrefix;
    private final Long timeout;

    public RedisCache(RedisTemplate<String, V> redisTemplate, String keyPrefix, Long timeout) {
        this.redisTemplate = redisTemplate;
        this.keyPrefix = keyPrefix;
        this.timeout = timeout;
    }

    public RedisCache(RedisTemplate<String, V> redisTemplate, String keyPrefix) {
        this.redisTemplate = redisTemplate;
        this.keyPrefix = keyPrefix;
        this.timeout = null;
    }

    @Override
    public V get(K key) throws CacheException {
        return redisTemplate.opsForValue().get(key(key));
    }

    @Override
    public V put(K key, V value) throws CacheException {
        if (timeout != null) {
            redisTemplate.opsForValue().set(key(key), value, timeout, TimeUnit.MILLISECONDS);
        } else {
            redisTemplate.opsForValue().set(key(key), value);
        }
        return value;
    }

    @Override
    public V remove(K key) throws CacheException {
        final V vl = redisTemplate.opsForValue().get(key(key));
        if (null != vl)
            redisTemplate.delete(key(key));
        return vl;
    }

    @Override
    public void clear() throws CacheException {
        Set<String> keys = redisTemplate.keys(keyPrefix + "*");
        if (keys != null && keys.size() > 0) {
            redisTemplate.delete(keys);
        }
    }

    @Override
    public int size() {
        return redisTemplate.keys(keyPrefix + "*").size();
    }

    @Override
    public Set<K> keys() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return redisTemplate.opsForValue().multiGet(redisTemplate.keys(keyPrefix + "*"));
    }

    private String key(K key) {
        return keyPrefix + key.toString();
    }
}
