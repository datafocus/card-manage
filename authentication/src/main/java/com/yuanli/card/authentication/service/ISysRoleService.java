package com.yuanli.card.authentication.service;


import com.yuanli.card.pojo.SysRole;

import java.util.List;

/**
 * Created by layne on 2018/5/23.
 */
public interface ISysRoleService {

    List<SysRole> findRolesByUserId(Long userId);

    void addSysRole(SysRole sysRole);

    void delSysRole(Long id);
}