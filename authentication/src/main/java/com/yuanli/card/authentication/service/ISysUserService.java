package com.yuanli.card.authentication.service;


import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.req.SysUserQueryReq;
import com.yuanli.card.req.UserReq;
import com.yuanli.card.req.UserReq0;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by layne on 2018/5/23.
 */
public interface ISysUserService {


    SysUser findByUserName(String username);

    //更新最后登录时间
    void updateUserLastLoginInfoById(Long userId);

    SysUser findById(Long userId);

    void save(SysUser sysUser);

    void login(SysUser sysUser);

    void delSysUser(Long id);

    List<SysUser> listChild(SysUserQueryReq req);

    List<SysUser> listAll();

    SysUser currentUser();

    List<Integer> getAllChildId();

    List<Integer> getChildId();

    void update(SysUser sysUser);
}
