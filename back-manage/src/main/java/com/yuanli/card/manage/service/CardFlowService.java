package com.yuanli.card.manage.service;

import com.yuanli.card.pojo.CardFlow;

import java.util.List;

public interface CardFlowService {

    List<CardFlow> getByIccid(String iccid);
}
