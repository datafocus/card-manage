package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.authentication.service.ISysUserService;
import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.manage.service.ChannelService;
import com.yuanli.card.manage.service.ComboRateService;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.manage.service.SysUserService;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.req.ComboAddReq;
import com.yuanli.card.req.ComboQueryReq;
import com.yuanli.card.res.ResponseBean;
import com.yuanli.card.utils.JsonMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by layne on 2018/11/26.
 */
@RestController
@RequestMapping("/combo")
public class ComboController {
    @Autowired
    private ComboService comboService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ChannelService channelService;
    @Autowired
    private ComboRateService comboRateService;

    @PostMapping("/add")
    public ResponseBean add(ComboAddReq req) {
        comboService.save(req);
        return ResponseBean.success();
    }

    @GetMapping("/query")
    public ResponseBean query(ComboQueryReq req) {
        req.setCustomerId(sysUserService.currentUser().getId().intValue());
        PageInfo pageInfo = comboService.query(req);
        Map<String, Object> result = new HashMap<>();
        result.put("total", pageInfo.getTotal());
        result.put("list", this.fillCombo(pageInfo.getList()));
        return ResponseBean.success().setData(result);
    }

    @GetMapping("/allCombo")
    public ResponseBean allCombo(){
        return ResponseBean.success().setData(comboService.listAll());
    }

    @GetMapping("/customerCombo")
    public ResponseBean customerCombo(){
        return ResponseBean.success().setData(comboService.listByCustomer(sysUserService.currentUser().getId().intValue()));
    }

    @PostMapping("/updateCost")
    public ResponseBean updateCost(String comboNo, BigDecimal cost) {
        if (StringUtils.isBlank(comboNo) || cost == null) {
            throw new ServiceException(1, "参数错误");
        }
        SysUser sysUser = sysUserService.currentUser();
        if (!sysUser.getUser_type().equals("ADMIN")) {
            throw new ServiceException(1, "您没有此操作权限");
        }
        ComboRate comboRate = comboRateService.getByComboAndCustomer(comboNo,sysUser.getId().intValue());
        comboRate.setSalePrice(cost);
        comboRateService.updateSalePrice(comboRate);
        return ResponseBean.success();
    }

    private List<Map<String, Object>> fillCombo(List<Combo> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        int userId = sysUserService.currentUser().getId().intValue();
        return list.stream().map(combo -> {
            Map<String, Object> map = JsonMapper.defaultMapper().json2Map(JsonMapper.defaultMapper().toJson(combo));
            Channel channel = channelService.findChannelByNo(combo.getChannelNo());
            map.put("channelName",channel==null?null:channel.getName());
            ComboRate comboRate = comboRateService.getByComboAndCustomer(combo.getComboNo(), userId);
            map.put("cost", comboRate == null ? null : comboRate.getSalePrice());
            return map;
        }).collect(Collectors.toList());
    }
}
