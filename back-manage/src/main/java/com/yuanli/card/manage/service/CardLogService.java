package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.CardLog;
import com.yuanli.card.req.CardLogQueryReq;

public interface CardLogService {
    PageInfo<CardLog> query(CardLogQueryReq req);
}
