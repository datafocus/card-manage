package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.Pool;
import com.yuanli.card.req.PoolQueryReq;

import java.util.List;

public interface PoolService {
    PageInfo<Pool> query(PoolQueryReq req);

    List<Pool> list(List<Integer> customerIds);

    void updatePoolName(Pool pool);

    Pool getByPoolCode(String poolCode);
}
