package com.yuanli.card.manage.service;

import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.utils.JsonMapper;
import com.yuanli.card.utils.SHA1;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Created by layne on 2018/12/6.
 */
public class QingdaoChannel {

    /**
     * 批量停开机（最多支持20个号码），
     *
     * @param ability
     * @param paramsIn
     *//*
    @Override
    public List<String> batchStopOpen(ChannelInterface.AbilitiesBean.AbilityBean ability, Map<String, Object> paramsIn) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        ability.getParams().forEach((k, v) -> params.set(k, (String) v));
        String stoptype = (String) paramsIn.get("stoptype");
        setToken(params, ability);
        params.set("stoptype", stoptype);
        params.set("runRule", (String) paramsIn.get("runRule"));
        String groupId = (String) ability.getParams().getOrDefault("groupId", "");
        String needPwd = "";
        String password = "";
        String certid = "";
        String changeReason = "";
        params.set("groupId", groupId);
        params.set("needPwd", needPwd);
        params.set("password", password);
        params.set("certid", certid);
        params.set("changeReason", changeReason);
        List<String> cards = (List<String>) paramsIn.get("subList");
        int limit = 20;
        if (cards.size() > limit) {
            List<String> result = new ArrayList<>();
            List<Future<List<String>>> futures = subList(cards, limit).stream()
                    .map(stringList -> executorService.submit(() -> singleBatchStopOpen(stringList, certid,
                            changeReason, groupId, needPwd, password, stoptype, ability, params)))
                    .collect(Collectors.toList());
            futures.forEach(f -> {
                try {
                    result.addAll(f.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });
            if (result.size() == 0) {
                throw new ServiceException(1, "更新失败");
            }
            return result.size() == cards.size() ? null : result;
        } else {
            List<String> result = singleBatchStopOpen(cards, certid, changeReason, groupId, needPwd, password,
                    stoptype, ability, params);
            if (result.size() == 0) {
                throw new ServiceException(1, "更新失败");
            }
            return result.size() == cards.size() ? null : result;
        }
    }

    private List<String> singleBatchStopOpen(List<String> cards, String certid,
                                             String changeReason, String groupId,
                                             String needPwd, String password,
                                             String stoptype, ChannelInterface.AbilitiesBean.AbilityBean ability,
                                             MultiValueMap<String, String> paramsIn) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(paramsIn);

        Set<Sublist> sublists = cards.stream().map(s -> new Sublist()
                .setCertid(certid)
                .setChangeReason(changeReason)
                .setGroupId(groupId)
                .setNeedPwd(needPwd)
                .setPassword(password)
                .setStoptype(stoptype)
                .setChangeReason(changeReason)
                .setTelnum(s))
                .collect(Collectors.toSet());
        params.set("subsList", JsonMapper.toJsonString(sublists));
        QingdaoRes res = invoke(QingdaoRes.class, ability.getUrl(), ability.getMediaType(), ability.getMethed(), params);
        logger.info("singleBatchStopOpen:{}", JsonMapper.toJsonString(res));
        if (res.res_code.equals("00")) {
            return cards;
        }
        return new ArrayList<>();
    }


    @Override
    public void cancelTestPeriod(ChannelInterface.AbilitiesBean.AbilityBean ability, Map<String, Object> paramsIn) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        ability.getParams().forEach((k, v) -> params.set(k, (String) v));
        setToken(params, ability);
        params.set("tel", (String) paramsIn.get("tel"));
        QingdaoRes res = invoke(QingdaoRes.class, ability.getUrl(), ability.getMediaType(), ability.getMethed(), params);
        logger.info("cancelTestPeriod:{}", JsonMapper.toJsonString(res));
        if (res.data == null) {
            //失败
            throw new ServiceException("取消测试期失败,返回:" + res.res_desc);
        }
    }

    protected void setToken(MultiValueMap<String, String> params, ChannelInterface.AbilitiesBean.AbilityBean ability) {
        if (Boolean.valueOf(ability.getIsEc())) {
            super.setToken(params, ability);
        } else {
            String appkey = (String) ability.getParams().get("appkey");
            String appsecret = (String) ability.getParams().get("appsecret");
            params.set("token", SHA1.getSHA1(appkey, appsecret));
        }
    }

    class QingdaoRes {

        private String res_code;
        private String res_desc;
        private Object data;

        public String getRes_code() {
            return res_code;
        }

        public void setRes_code(String res_code) {
            this.res_code = res_code;
        }

        public String getRes_desc() {
            return res_desc;
        }

        public void setRes_desc(String res_desc) {
            this.res_desc = res_desc;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }
    }

    class Sublist {

        private String telnum;
        private String stoptype;
        private String groupId;
        private String needPwd;
        private String password;
        private String certid;
        private String changeReason;

        public String getTelnum() {
            return telnum;
        }

        public Sublist setTelnum(String telnum) {
            this.telnum = telnum;
            return this;
        }

        public String getStoptype() {
            return stoptype;
        }

        public Sublist setStoptype(String stoptype) {
            this.stoptype = stoptype;
            return this;
        }

        public String getGroupId() {
            return groupId;
        }

        public Sublist setGroupId(String groupId) {
            this.groupId = groupId;
            return this;
        }

        public String getNeedPwd() {
            return needPwd;
        }

        public Sublist setNeedPwd(String needPwd) {
            this.needPwd = needPwd;
            return this;
        }

        public String getPassword() {
            return password;
        }

        public Sublist setPassword(String password) {
            this.password = password;
            return this;
        }

        public String getCertid() {
            return certid;
        }

        public Sublist setCertid(String certid) {
            this.certid = certid;
            return this;
        }

        public String getChangeReason() {
            return changeReason;
        }

        public Sublist setChangeReason(String changeReason) {
            this.changeReason = changeReason;
            return this;
        }
    }
*/
}
