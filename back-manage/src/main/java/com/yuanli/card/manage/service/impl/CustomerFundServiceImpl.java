package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.CustomerFundService;
import com.yuanli.card.mapper.CustomerFundMapper;
import com.yuanli.card.pojo.CustomerFund;
import com.yuanli.card.req.CustomerFundQueryReq;
import com.yuanli.card.req.CustomerRechargeReq;
import com.yuanli.card.res.FundCountAmountRes;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class CustomerFundServiceImpl implements CustomerFundService {
    @Resource
    private CustomerFundMapper customerFundMapper;

    @Override
    public void recharge(CustomerRechargeReq req) {
        CustomerFund customerFund = new CustomerFund();
        customerFund.setAmount(req.getAmount());
        customerFund.setCustomerId(req.getRechargeCustomerId());
        customerFund.setType(1);
        customerFund.setCreateTime(new Date());
        customerFund.setRemark("充值");
        customerFundMapper.save(customerFund);
    }

    @Override
    public PageInfo<CustomerFund> query(CustomerFundQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(), "create_time desc");
        return customerFundMapper.query(req).toPageInfo();
    }

    @Override
    public FundCountAmountRes countAmount(CustomerFundQueryReq req) {
        return customerFundMapper.countAmount(req);
    }
}
