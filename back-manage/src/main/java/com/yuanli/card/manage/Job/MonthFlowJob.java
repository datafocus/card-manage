package com.yuanli.card.manage.Job;

import com.yuanli.card.manage.service.CardFreshService;
import com.yuanli.card.mapper.CardMapper;
import com.yuanli.card.mapper.PoolMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class MonthFlowJob {

    @Resource
    private PoolMapper poolMapper;
    @Resource
    private CardMapper cardMapper;
    @Resource
    private CardFreshService cardFreshService;


    /*
        卡月流量保存
        月底最后一天晚上8点执行
     */
    @Scheduled(cron="0 0 20 L * ?")
    public void cardFlowStart(){
        cardFreshService.cardFlowFresh(cardMapper.listAllIccid());
    }

    /*
        流量池月流量保存
        月底最后一天晚上8点执行
     */
    @Scheduled(cron="0 0 20 L * ?")
    public void poolFlowStart(){
        cardFreshService.poolFlowFresh(poolMapper.listAllPoolNo());
    }
}
