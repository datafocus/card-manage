package com.yuanli.card.manage.service.impl;

import com.yuanli.card.manage.service.CardFlowService;
import com.yuanli.card.mapper.CardFlowMapper;
import com.yuanli.card.pojo.CardFlow;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CardFlowServiceImpl implements CardFlowService {

    @Resource
    private CardFlowMapper cardFlowMapper;

    @Override
    public List<CardFlow> getByIccid(String iccid) {
        return cardFlowMapper.getByIccid(iccid);
    }
}
