package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.authentication.service.ISysUserService;
import com.yuanli.card.manage.service.ChannelInterfaceService;
import com.yuanli.card.manage.service.ChannelService;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.manage.service.PoolService;
import com.yuanli.card.pojo.CardFlow;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.Pool;
import com.yuanli.card.req.PoolQueryReq;
import com.yuanli.card.res.ResponseBean;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/pool")
public class PoolController {

    @Resource
    private PoolService poolService;
    @Resource
    private ISysUserService sysUserService;
    @Resource
    private ComboService comboService;
    @Resource
    private ChannelService channelService;
    @Resource
    private ChannelInterfaceService channelInterfaceService;

    @GetMapping("/query")
    public ResponseBean query(PoolQueryReq req) {
        Map<String, Object> data = new HashMap<>();
        List<Integer> customerIds =  new ArrayList<>();
        customerIds.add(sysUserService.currentUser().getId().intValue());
        List<Integer> childIds = sysUserService.getAllChildId();
        if (childIds != null&&!childIds.isEmpty()) {
            customerIds.addAll(childIds);
        }
        req.setCustomerIds(customerIds);
        PageInfo<Pool> pageInfo = poolService.query(req);
        data.put("total", pageInfo.getTotal());
        data.put("list", this.fillPool(pageInfo.getList()));
        return ResponseBean.success().setData(data);
    }

    @GetMapping("/list")
    public ResponseBean list() {
        Map<String, Object> data = new HashMap<>();
        List<Integer> customerIds =  new ArrayList<>();
        customerIds.add(sysUserService.currentUser().getId().intValue());
        List<Integer> childIds = sysUserService.getAllChildId();
        if (childIds != null&&!childIds.isEmpty()) {
            customerIds.addAll(childIds);
        }
        return ResponseBean.success().setData(poolService.list(customerIds));
    }

    @PostMapping("/update")
    private ResponseBean update(Pool pool) {
        if (StringUtils.isBlank(pool.getPoolName()) || StringUtils.isBlank(pool.getPoolCode())) {
            return ResponseBean.response().setCode(-1).setMessage("参数错误");
        }
        poolService.updatePoolName(pool);
        return ResponseBean.success();
    }

    private List<Map<String, Object>> fillPool(List<Pool> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        Map<String, CardFlow> flowMap = channelInterfaceService.batchQueryRealtimeGprsInfo(list.stream().map(Pool::getPoolNo).collect(Collectors.toList()));
        List<Map<String, Object>> result = list.parallelStream().map(pool -> {
            Map<String, Object> map = new HashMap<>();
            map.put("poolCode", pool.getPoolCode());
            map.put("poolNo", pool.getPoolNo());
            map.put("poolName", pool.getPoolName());
            Channel channel = channelService.findChannelByNo(pool.getChannelCode());
            map.put("channelName", channel==null?null:channel.getName());
            Combo combo = comboService.getByComboNo(pool.getComboCode());
            map.put("comboName", combo==null?null:combo.getComboName());
            CardFlow cardFlow = flowMap.get(pool.getPoolNo());
            if (cardFlow != null) {
                map.put("used", cardFlow.getUsed()+"M");
                map.put("total", cardFlow.getTotal()+"M");
                map.put("leftPercent", (cardFlow.getTotal()-cardFlow.getUsed())*100/cardFlow.getTotal()+"%");
            }
            return map;
        }).collect(Collectors.toList());
        return result;
    }
}
