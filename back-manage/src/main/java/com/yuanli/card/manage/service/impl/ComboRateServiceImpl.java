package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.ComboRateService;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.mapper.ComboMapper;
import com.yuanli.card.mapper.ComboRateMapper;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.req.ComboAddReq;
import com.yuanli.card.req.ComboQueryReq;
import com.yuanli.card.req.ComboRateAddReq;
import com.yuanli.card.req.ComboRateQueryReq;
import com.yuanli.card.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ComboRateServiceImpl implements ComboRateService {
    @Autowired
    private ComboRateMapper comboRateMapper;


    public void save(ComboRateAddReq req) {
        ComboRate comboRate = new ComboRate();
        comboRate.setComboNo(req.getComboNo());
        comboRate.setCustomerId(req.getCustomerId());
        comboRate.setSalePrice(req.getSalePrice());
        comboRate.setCreateTime(new Date());
        comboRate.setUpdateTime(new Date());
        comboRateMapper.save(comboRate);
    }

    /**
     * 查询套餐费率 当前用户建立
     *
     * @param req
     * @return
     */
    public PageInfo query(ComboRateQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(),"create_time desc");
        Page<ComboRate> page = comboRateMapper.query(req);
        return page.toPageInfo();
    }

    @Override
    public void updateSalePrice(ComboRate comboRate) {
        comboRateMapper.updateSalePrice(comboRate);
    }

    @Override
    public ComboRate getByComboAndCustomer(String comboNo, Integer customerId) {
        return comboRateMapper.getByComboAndCustomer(comboNo,customerId);
    }
}
