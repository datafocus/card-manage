package com.yuanli.card.manage.controller;

import com.yuanli.card.manage.service.SysUserService;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.res.ResponseBean;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by layne on 2018/11/13.
 */
@RestController
public class LoginController {

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/login")
    public ResponseBean login(SysUser sysUser) {
        sysUserService.login(sysUser);
        return ResponseBean.success();
    }

    /**
     * @return
     */
    @GetMapping("/currentUser")
    public ResponseBean<SysUser> currentUser() {
        SysUser userSession = (SysUser) SecurityUtils.getSubject().getSession().getAttribute("userSession");
        return ResponseBean.success().setData(userSession);
    }

    @PostMapping("/logout")
    public ResponseBean logout() {
        SecurityUtils.getSubject().logout();
        return ResponseBean.success();
    }

    @GetMapping("/405")
    public ResponseBean page405() {
        return ResponseBean.response().setCode(405).setData("未登录");
    }
}
