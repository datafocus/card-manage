package com.yuanli.card.manage.service;

import com.yuanli.card.authentication.service.ISysRoleService;
import com.yuanli.card.mapper.SysRoleMapper;
import com.yuanli.card.pojo.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by layne on 2018/11/13.
 */
@Service
public class SysRoleService implements ISysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public List<SysRole> findRolesByUserId(Long userId) {
        return null;
    }

    @Override
    public void addSysRole(SysRole sysRole) {
        sysRoleMapper.save(sysRole);
    }

    @Override
    public void delSysRole(Long id) {
        sysRoleMapper.delByid(id);
    }
}
