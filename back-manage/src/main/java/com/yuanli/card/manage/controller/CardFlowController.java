package com.yuanli.card.manage.controller;

import com.yuanli.card.manage.service.CardFlowService;
import com.yuanli.card.res.ResponseBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;

@RestController
@RequestMapping("/cardFlow")
public class CardFlowController {

    @Resource
    private CardFlowService cardFlowService;

    @GetMapping("/getByIccid")
    public ResponseBean getByIccid(@RequestParam String iccid) {
        return ResponseBean.success().setData(cardFlowService.getByIccid(iccid));
    }
}
