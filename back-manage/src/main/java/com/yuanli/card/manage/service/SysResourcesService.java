package com.yuanli.card.manage.service;

import com.yuanli.card.authentication.service.ISysResourcesService;
import com.yuanli.card.pojo.SysResources;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by layne on 2018/11/13.
 */
@Service
public class SysResourcesService implements ISysResourcesService {
    @Override
    public List<SysResources> listUrlAndPermission() {
        return null;
    }

    @Override
    public List<SysResources> listByUserId(Long userId) {
        return null;
    }

    @Override
    public List<SysResources> findAll() {
        return null;
    }

    @Override
    public void addSysRes(SysResources sysResources) {

    }
}
