package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.EnterStoreService;
import com.yuanli.card.mapper.EnterStoreMapper;
import com.yuanli.card.pojo.EnterStore;
import com.yuanli.card.req.EnterStoreQueryReq;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class EnterStoreServiceImpl implements EnterStoreService {

    @Resource
    private EnterStoreMapper enterStoreMapper;

    @Override
    public PageInfo<EnterStore> query(EnterStoreQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(), "create_time desc");
        return enterStoreMapper.query(req).toPageInfo();
    }
}
