package com.yuanli.card.manage.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.mapper.ChannelMapper;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.req.ChannelListReq;
import com.yuanli.card.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by layne on 2018/11/15.
 */
@Service
public class ChannelService {

    @Autowired
    private ChannelMapper channelMapper;

    /**
     * 添加通道
     *
     * @param channel
     */
    public void addChannel(Channel channel) {
        //通道编码
        channel.setChannle_no(CommonUtil.randomCode());
        channel.setStatus(1);
        channelMapper.save(channel);
    }

    public List<Channel> channelNameList() {
        return channelMapper.finfAllChannelNameAndNo();
    }

    public List<Channel> allChannel() {
        return channelMapper.finfAllChannelNameAndNo();
    }

    public PageInfo<Channel> channelList(ChannelListReq listReq) {
        Page<Channel> channels = channelMapper.findAll(listReq);
        return channels.toPageInfo();
    }

    public Channel findChannelByNo(String no) {
        return channelMapper.findChannelByNo(no);
    }

    public void changeChannelStatus(Long id) {
        int channelStatusById = channelMapper.findChannelStatusById(id);
        int status = channelStatusById == 0 ? 1 : 0;
        channelMapper.changeChannelStatus(status, id);
    }


}
