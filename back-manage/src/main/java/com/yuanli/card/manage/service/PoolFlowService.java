package com.yuanli.card.manage.service;

import com.yuanli.card.pojo.PoolFlow;

import java.util.List;

public interface PoolFlowService {
    List<PoolFlow> getByPoolCode(String poolCode);
}
