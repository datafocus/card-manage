package com.yuanli.card.manage.service;

import com.yuanli.card.enumeration.CardPeriodEnum;
import com.yuanli.card.enumeration.CardStatusEnum;
import com.yuanli.card.exception.Exceptions;
import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.pojo.Card;
import com.yuanli.card.pojo.CardFlow;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.yuanli.card.manage.service.ChannelInterfaceHelper.getAbility;

/**
 * Created by layne on 2018/11/15.
 */
@Service
public class ChannelInterfaceService {

    @Autowired
    private CardService cardService;

    /**
     * 批量停复机
     *
     * @param cardNos   卡号
     * @param stopType  停复机类型 StopSubs：申请停机 OpenSubs：申请开机
     * @param runRule   生效规则
     * @param cardLogId 卡操作id
     */
    public List<String> batchStopOpen(List<String> cardNos, String stopType, String runRule, Integer cardLogId) {
        //根据卡号查询通道
        if (cardNos == null || cardNos.size() == 0) {
            throw new ServiceException(999, "卡号不能为空");
        }
        String no = cardNos.get(0);
        Card card = Optional.ofNullable(cardService.getByIccid(no))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));

        ChannelInterface.AbilitiesBean.AbilityBean ability = getAbility(card.getChannelCode(),
                "batchStopOpen");
        Map<String, Object> params = ability.getParams();
        params.put("stopType", stopType);
        params.put("runRule", runRule);
        params.put("subList", cardNos);
        return ability.instance().batchStopOpen(ability, params);
    }

    /**
     * 取消测试期
     *
     * @param cardNos
     */
    public void cancelTestPeriod(List<String> cardNos, Integer cardLogId) {
        if (cardNos == null || cardNos.size() == 0) {
            throw new ServiceException(999, "卡号不能为空");
        }
        String cardNo = cardNos.get(0);
        Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));

        ChannelInterface.AbilitiesBean.AbilityBean cancelTestPeriod = getAbility(byIccid.getChannelCode(),
                "cancelTestPeriod");
        cardNos.forEach(s -> {
            Map<String, Object> params = cancelTestPeriod.getParams();
            params.put("tel", s);
            cancelTestPeriod.instance().cancelTestPeriod(cancelTestPeriod, params);
        });
    }

    /**
     * 批量查询卡状态信息
     *
     * @param cardNos
     * @return
     */
    public Map<String, CardStatusEnum> batchQueryUserStatus(List<String> cardNos, String channelCode) {
        if (StringUtils.isBlank(channelCode)) {
            if (cardNos == null || cardNos.size() == 0) {
                throw new ServiceException(999, "卡号不能为空");
            }
            String cardNo = cardNos.get(0);
            Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                    .orElseThrow(() -> new ServiceException(1, "通道未分配"));

            channelCode = byIccid.getChannelCode();
        }
        ChannelInterface.AbilitiesBean.AbilityBean queryUserStatus = Optional.ofNullable(getAbility(channelCode,
                "queryUserStatus")).orElseThrow(() -> new ServiceException(999, "获取通道能力出错"));

        Map<String, CardStatusEnum> result = new HashMap<>();
        cardNos.parallelStream().forEach(s -> {
            Map<String, Object> params = queryUserStatus.getParams();
            params.put("tel", s);
            CardStatusEnum statusEnum = queryUserStatus.instance().queryUserStatus(queryUserStatus, params);
            result.put(s, statusEnum);
        });
        return result;
    }

    /**
     * 批量查询gprs状态信息
     *
     * @param cardNos
     * @return
     */
    public Map<String, Integer> batchQueryGprsStatus(List<String> cardNos) {
        if (cardNos == null || cardNos.size() == 0) {
            throw new ServiceException(999, "卡号不能为空");
        }
        String cardNo = cardNos.get(0);
        Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));
        ChannelInterface.AbilitiesBean.AbilityBean batchQueryGprsStatus = getAbility(byIccid.getChannelCode(),
                "batchQueryGprsStatus");
        Map<String, Object> map = new HashMap<>();
        map.put("iccids", cardNos);
        return batchQueryGprsStatus.instance().batchQueryGprsStatus(batchQueryGprsStatus, map);
    }

    /**
     * 批量查询卡生命期信息
     *
     * @param cardNos
     * @return
     */
    public Map<String, CardPeriodEnum> batchQueryCardLifeCycle(List<String> cardNos, String channelCode) {
        if (StringUtils.isBlank(channelCode)) {
            if (cardNos == null || cardNos.size() == 0) {
                throw new ServiceException(999, "卡号不能为空");
            }
            String cardNo = cardNos.get(0);
            Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                    .orElseThrow(() -> new ServiceException(1, "通道未分配"));
            channelCode = byIccid.getChannelCode();
        }
        ChannelInterface.AbilitiesBean.AbilityBean batchQueryCardLifeCycle = getAbility(channelCode,
                "batchQueryCardLifeCycle");

        Map<String, Object> map = new HashMap<>();
        map.put("iccids", cardNos);
        return batchQueryCardLifeCycle.instance().batchQueryCardLifeCycle(batchQueryCardLifeCycle, map);
    }

    /**
     * 批量查询当月套餐内流量使用信息
     * 批量查询某个月流量使用信息
     *
     * @param cardNos
     * @param date
     * @return
     */
    public Map<String, CardFlow> batchGprsUsedByDate(List<String> cardNos, Date date) {
        String cardNo = cardNos.get(0);
        Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));

        ChannelInterface.AbilitiesBean.AbilityBean batchGprsUsedByDate = getAbility(byIccid.getChannelCode(),
                "batchGprsUsedByDate");
        Map<String, Object> map = new HashMap<>();
        map.put("iccids", cardNos);
        map.put("query_date", date);
        return batchGprsUsedByDate.instance().batchGprsUsedByDate(batchGprsUsedByDate, map);
    }

    /**
     * 批量查询当月共享流量使用信息
     *
     * @param cardNos
     * @return
     */
    public Map<String, CardFlow> batchQueryGprsShare(List<String> cardNos) {
        String cardNo = cardNos.get(0);
        Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));

        ChannelInterface.AbilitiesBean.AbilityBean batchQueryGprsShare = getAbility(byIccid.getChannelCode(),
                "batchQueryGprsShare");
        Map<String, Object> map = new HashMap<>();
        map.put("iccids", cardNos);
        return batchQueryGprsShare.instance().batchQueryGprsShare(batchQueryGprsShare, map);
    }

    /**
     * 集团客户可以根据ICCID，批量按套餐查询所属物联卡当月套餐内GPRS流量实时使用情况
     *
     * @param cardNos
     * @return
     */
    public Map<String, CardFlow> batchQueryRealtimeGprsInfo(List<String> cardNos) {
        if (cardNos == null || cardNos.size() < 0) {
            throw new ServiceException(999, "卡号不能为空");
        }
        String cardNo = cardNos.get(0);
        Card byIccid = Optional.ofNullable(cardService.getByIccid(cardNo))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));

        ChannelInterface.AbilitiesBean.AbilityBean batchQueryRealtimeGprsInfo = getAbility(byIccid.getChannelCode(),
                "batchQueryRealtimeGprsInfo");

        Map<String, Object> map = new HashMap<>();
        map.put("iccids", cardNos);

        return batchQueryRealtimeGprsInfo.instance().batchQueryRealtimeGprsInfo(batchQueryRealtimeGprsInfo, map);

    }

    /**
     * 集团客户可查询所属物联卡当月套餐内GPRS流量使用情况
     *
     * @param iccid
     */
    public CardFlow gprsRealtimeInfo(String iccid) {

        Exceptions.requireNonNull(iccid, "iccid不能为空");

        Card byIccid = Optional.ofNullable(cardService.getByIccid(iccid))
                .orElseThrow(() -> new ServiceException(1, "通道未分配"));

        Map<String, Object> params = new HashMap<>();
        params.put("iccid", iccid);
        ChannelInterface.AbilitiesBean.AbilityBean gprsRealtimeInfo = getAbility(byIccid.getChannelCode(),
                "gprsRealtimeInfo");

        return gprsRealtimeInfo.instance().gprsRealtimeInfo(gprsRealtimeInfo, params);
    }
    //批量查询卡激活时间？
}
