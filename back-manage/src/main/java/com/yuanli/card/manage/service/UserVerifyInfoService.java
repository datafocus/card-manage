package com.yuanli.card.manage.service;

import com.yuanli.card.mapper.UserVerifyInfoMapper;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.pojo.UserVerifyInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by layne on 2018/11/22.
 */
@Service
public class UserVerifyInfoService {

    @Autowired
    private UserVerifyInfoMapper userVerifyInfoMapper;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 提交实名信息
     *
     * @param userVerifyInfo
     */
    public void submit(UserVerifyInfo userVerifyInfo) {
        SysUser sysUser = sysUserService.currentUser();
        if (!StringUtils.isNoneEmpty(userVerifyInfo.getFront(), userVerifyInfo.getBack(), userVerifyInfo.getPermit())) {
            //不能有空
        }
        //设置状态为审核中
        userVerifyInfo.setStatus(0);
        userVerifyInfo.setUser_id(sysUser.getId());
        userVerifyInfoMapper.save(userVerifyInfo);
    }


    /**
     * 修改审核状态
     *
     * @param id
     * @param status
     */
    public void check(Long id, int status) {
        status = status == 1 ? 1 : 2;
        userVerifyInfoMapper.changeStatus(id, status);
    }
}
