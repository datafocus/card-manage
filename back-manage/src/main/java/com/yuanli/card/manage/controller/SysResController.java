package com.yuanli.card.manage.controller;

import com.yuanli.card.authentication.service.ISysResourcesService;
import com.yuanli.card.pojo.SysResources;
import com.yuanli.card.res.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by layne on 2018/11/14.
 */
@RestController
@RequestMapping("/sysRes")
public class SysResController {

    @Autowired
    private ISysResourcesService sysResourcesService;

    @PostMapping("/addSysRes")
    public ResponseBean addSysRes(@RequestBody SysResources sysResources) {
        sysResourcesService.addSysRes(sysResources);
        return ResponseBean.success();
    }
}
