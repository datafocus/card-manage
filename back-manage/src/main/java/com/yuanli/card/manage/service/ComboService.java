package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.req.ComboAddReq;
import com.yuanli.card.req.ComboQueryReq;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface ComboService {
    void save(ComboAddReq req);

    PageInfo query(ComboQueryReq req);

    Combo getByComboNo(String comboNo);

    List<Combo> listAll();

    List<Map<String,Object>> listByCustomer(Integer customerId);
}
