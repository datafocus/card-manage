package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.CardLogService;
import com.yuanli.card.mapper.CardLogMapper;
import com.yuanli.card.pojo.CardLog;
import com.yuanli.card.req.CardLogQueryReq;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CardLogServiceImpl implements CardLogService {

    @Resource
    private CardLogMapper cardLogMapper;

    @Override
    public PageInfo<CardLog> query(CardLogQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(), "create_time desc");
        return cardLogMapper.query(req).toPageInfo();
    }
}
