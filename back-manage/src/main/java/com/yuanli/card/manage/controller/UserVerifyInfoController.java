package com.yuanli.card.manage.controller;

import com.yuanli.card.manage.service.UserVerifyInfoService;
import com.yuanli.card.pojo.UserVerifyInfo;
import com.yuanli.card.res.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by layne on 2018/11/22.
 */
@RestController
@RequestMapping("/userVerifyInfo")
public class UserVerifyInfoController {

    @Autowired
    private UserVerifyInfoService userVerifyInfoService;

    @PostMapping("/submit")
    public ResponseBean submit(@RequestBody UserVerifyInfo userVerifyInfo) {
        userVerifyInfoService.submit(userVerifyInfo);
        return ResponseBean.success();
    }

    /**
     * 审核
     *
     * @param id
     * @param status 1 通过,2 不通过
     * @return
     */
    @PostMapping("/check")
    public ResponseBean check(Long id, Integer status) {
        userVerifyInfoService.check(id, status);
        return ResponseBean.success();
    }
}
