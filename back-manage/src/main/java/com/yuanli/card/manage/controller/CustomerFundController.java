package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.authentication.service.ISysUserService;
import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.manage.service.CustomerFundService;
import com.yuanli.card.pojo.CustomerFund;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.req.CustomerFundQueryReq;
import com.yuanli.card.req.CustomerRechargeReq;
import com.yuanli.card.res.FundCountAmountRes;
import com.yuanli.card.res.ResponseBean;
import com.yuanli.card.utils.JsonMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/customerFund")
public class CustomerFundController {

    @Resource
    private CustomerFundService customerFundService;
    @Resource
    private ISysUserService sysUserService;

    @PostMapping("/recharge")
    public ResponseBean recharge(CustomerRechargeReq req) {
        //判断充值客户是否当前客户直接下级
        List<Integer> childIds = sysUserService.getChildId();
        if (childIds == null || childIds.isEmpty()||!childIds.contains(req.getRechargeCustomerId())) {
            throw new ServiceException(1, "充值客户不存在");
        }
        customerFundService.recharge(req);
        return ResponseBean.success();
    }

    /*
        流水记录查询
        type=1 充值 type=2扣款
     */
    @GetMapping("/query")
    public ResponseBean query(CustomerFundQueryReq req) {
        Map<String, Object> data = new HashMap<>();
        if (req.getQueryType() == 1) {//平台
            req.setFundCustomerId(sysUserService.currentUser().getId().intValue());//设置当前customerid
        }else{//客户
            if (req.getFundCustomerId() == null) {
                List<Integer> customerIds = sysUserService.getChildId();
                if (customerIds == null || customerIds.isEmpty()) {
                    data.put("total", 0);
                    data.put("list", Collections.emptyList());
                    return ResponseBean.success().setData(data);
                }
                req.setCustomerIds(customerIds);
            }
        }
        PageInfo<CustomerFund> pageInfo = customerFundService.query(req);
        data.put("total", pageInfo.getTotal());
        data.put("list", this.fillCustomerFund(pageInfo.getList()));

        FundCountAmountRes amountRes = customerFundService.countAmount(req);
        if (amountRes != null) {
            data.put("rechargeAmount", amountRes.getRechargeAmount());
            data.put("consumeAmount", amountRes.getConsumeAmount());
            data.put("balance", amountRes.getRechargeAmount().subtract(amountRes.getConsumeAmount()));
        }
        return ResponseBean.success().setData(data);
    }

    private List<Map<String, Object>> fillCustomerFund(List<CustomerFund> list) {
        if (list == null) {
            return null;
        }
        return list.parallelStream().map(customerFund -> {
            Map<String, Object> map = JsonMapper.defaultMapper().json2Map(JsonMapper.defaultMapper().toJson(customerFund));
            SysUser user = sysUserService.findById(customerFund.getCustomerId().longValue());
            map.put("customerName",user==null?null:user.getNickname());
            return map;
        }).collect(Collectors.toList());
    }
}
