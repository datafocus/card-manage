package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.CustomerFund;
import com.yuanli.card.req.CustomerFundQueryReq;
import com.yuanli.card.req.CustomerRechargeReq;
import com.yuanli.card.res.FundCountAmountRes;

public interface CustomerFundService {
    void recharge(CustomerRechargeReq req);

    PageInfo<CustomerFund> query(CustomerFundQueryReq req);

    FundCountAmountRes countAmount(CustomerFundQueryReq req);
}
