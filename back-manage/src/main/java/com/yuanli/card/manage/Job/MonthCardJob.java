package com.yuanli.card.manage.Job;

import com.yuanli.card.manage.service.CardFreshService;
import com.yuanli.card.mapper.CardMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;

@Component
public class MonthCardJob {

    @Resource
    private CardMapper cardMapper;
    @Resource
    private CardFreshService cardFreshService;


    /*
        更新卡状态
        每天凌晨6点执行
        除去月初第一天
        包含异常卡
     */
    @Scheduled(cron = "0 0 6 * * ?")
    public void cardPeriodStart() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        if (calendar.get(Calendar.DAY_OF_MONTH) == 1) {
            return;
        }
        cardFreshService.cardPeriodFresh(cardMapper.listAllIccid(),1);
    }
}
