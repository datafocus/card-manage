package com.yuanli.card.manage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by layne on 2018/11/9.
 */
@SpringBootApplication(scanBasePackages = "com.yuanli.card")
public class ManageApp {
    public static void main(String[] args) {
        SpringApplication.run(ManageApp.class, args);
    }
}
