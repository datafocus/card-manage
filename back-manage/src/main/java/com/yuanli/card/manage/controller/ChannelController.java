package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.ChannelService;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.req.ChannelListReq;
import com.yuanli.card.res.PageRes;
import com.yuanli.card.res.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by layne on 2018/11/15.
 */
@RestController
@RequestMapping("/channel")
public class ChannelController {

    @Autowired
    private ChannelService channelService;

    @PostMapping("/addChannel")
    public ResponseBean addChannel(Channel channel) {
        channelService.addChannel(channel);
        return ResponseBean.success();
    }


    @GetMapping("/channelList")
    public ResponseBean<List<Channel>> channelList(ChannelListReq listReq) {
        PageInfo<Channel> pageInfo = channelService.channelList(listReq);
        return ResponseBean.success().setData(new PageRes<Channel>(pageInfo));
    }

    @GetMapping("/allChannel")
    public ResponseBean<List<Channel>> allChannel() {
        return ResponseBean.success().setData(channelService.allChannel());
    }


    /**
     * 通道停用启用
     *
     * @param id
     * @return
     */
    @PostMapping("/changeChannelStatus")
    public ResponseBean changeChannelStatus(Long id) {
        channelService.changeChannelStatus(id);
        return ResponseBean.success();
    }

}
