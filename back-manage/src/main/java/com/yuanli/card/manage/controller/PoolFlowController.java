package com.yuanli.card.manage.controller;

import com.yuanli.card.manage.service.PoolFlowService;
import com.yuanli.card.res.ResponseBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;

@RestController
@RequestMapping("/poolFlow")
public class PoolFlowController {

    @Resource
    private PoolFlowService poolFlowService;

    @GetMapping("/getByPoolCode")
    public ResponseBean getByPoolCode(@RequestParam String poolCode) {
        return ResponseBean.success().setData(poolFlowService.getByPoolCode(poolCode));
    }
}
