package com.yuanli.card.manage.Job;

import com.yuanli.card.manage.service.CardFreshService;
import com.yuanli.card.manage.service.CardService;
import com.yuanli.card.mapper.CardMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class MonthOrderJob {
    @Resource
    private CardMapper cardMapper;
    @Resource
    private CardFreshService cardFreshService;
    @Resource
    private CardService cardService;

    /*
        月初第一天凌晨4点执行，不包含异常卡
        1.同步生命期
        2.生成订单
     */
    @Scheduled(cron="0 0 4 1 * ?")
    public void autoOrderStart(){
        //todo 异常处理 如何避免从沉默期到使用期卡的扣费 在出售已扣费 确保通道状态已更新
        cardFreshService.cardPeriodFresh(cardMapper.listAllIccid(),1);
        cardService.autoOrder();
    }
}
