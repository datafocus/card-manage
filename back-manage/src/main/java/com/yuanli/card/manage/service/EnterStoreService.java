package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.EnterStore;
import com.yuanli.card.req.EnterStoreQueryReq;

public interface EnterStoreService {
    PageInfo<EnterStore> query(EnterStoreQueryReq req);
}
