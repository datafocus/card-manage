package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.manage.service.ComboRateService;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.manage.service.SysUserService;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.req.ComboRateAddReq;
import com.yuanli.card.req.ComboRateQueryReq;
import com.yuanli.card.res.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by layne on 2018/11/26.
 */
@RestController
@RequestMapping("/comboRate")
public class ComboRateController {
    @Autowired
    private ComboRateService comboRateService;
    @Resource
    private SysUserService sysUserService;
    @Resource
    private ComboService comboService;

    @PostMapping("/add")
    public ResponseBean add(ComboRateAddReq req) {
        comboRateService.save(req);
        return ResponseBean.success();
    }

    @PostMapping("/updateSalePrice")
    public ResponseBean updateSalePrice(ComboRate comboRate) {
        if (comboRate.getId() == null || comboRate.getSalePrice() == null) {
            throw new ServiceException(1, "参数错误");
        }
        comboRateService.updateSalePrice(comboRate);
        return ResponseBean.success();
    }

    @GetMapping("/query")
    public ResponseBean query(ComboRateQueryReq req) {
        Map<String, Object> result = new HashMap<>();
        if (req.getRateCustomerId() == null) {
            List<Integer> childIds = sysUserService.getChildId();
            if (childIds == null || childIds.isEmpty()) {
                result.put("total", 0);
                result.put("list", Collections.emptyList());
                return ResponseBean.success().setData(result);
            }
            req.setRateCustomerIds(sysUserService.getChildId());
        }
        PageInfo pageInfo = comboRateService.query(req);
        result.put("total", pageInfo.getTotal());
        result.put("list", this.fillComboRate(pageInfo.getList()));
        return ResponseBean.success().setData(result);
    }

    private List<Map<String, Object>> fillComboRate(List<ComboRate> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.stream().map(comboRate -> {
            Map<String, Object> map = new HashMap<>();
            map.put("id", comboRate.getId());
            Combo combo = comboService.getByComboNo(comboRate.getComboNo());
            map.put("comboName", combo.getComboName());
            map.put("comboSize", combo.getSize());
            map.put("comboType", combo.getType());
            map.put("customerName", sysUserService.findById(comboRate.getCustomerId().longValue()).getNickname());
            map.put("salePrice", comboRate.getSalePrice());
            return map;
        }).collect(Collectors.toList());
    }
}
