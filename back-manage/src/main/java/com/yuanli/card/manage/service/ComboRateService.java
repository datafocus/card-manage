package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.req.ComboRateAddReq;
import com.yuanli.card.req.ComboRateQueryReq;

public interface ComboRateService {
    void save(ComboRateAddReq req);

    PageInfo query(ComboRateQueryReq req);

    void updateSalePrice(ComboRate comboRate);

    ComboRate getByComboAndCustomer(String comboNo, Integer customerId);
}
