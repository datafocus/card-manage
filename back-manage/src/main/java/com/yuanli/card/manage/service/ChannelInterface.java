package com.yuanli.card.manage.service;

import java.util.List;
import java.util.Map;

/**
 * Created by layne on 2018/11/15.
 * 通道接口
 */
public class ChannelInterface {


    /**
     * channelName : qingdaoyidong
     * abilities : [{"abilityName":"batchStopOpen","ability":{"url":"http://223.99.141.141:10110/sdiot/bossopen/batchStopOpenSubs","params":{"appkey":""},"methed":"POST","mediatype":"application/json","isEc":"false","tokenClass":"com.yuanli.card.manage.service.QingdaoChannel"}}]
     */

    private String channelName;
    private List<AbilitiesBean> abilities;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public List<AbilitiesBean> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<AbilitiesBean> abilities) {
        this.abilities = abilities;
    }

    public static class AbilitiesBean {
        /**
         * abilityName : batchStopOpen
         * ability : {"url":"http://223.99.141.141:10110/sdiot/bossopen/batchStopOpenSubs","params":{"appkey":""},"methed":"POST","mediatype":"application/json","isEc":"false","tokenClass":"com.yuanli.card.manage.service.QingdaoChannel"}
         */

        private String abilityName;
        private AbilityBean ability;

        public String getAbilityName() {
            return abilityName;
        }

        public void setAbilityName(String abilityName) {
            this.abilityName = abilityName;
        }

        public AbilityBean getAbility() {
            return ability;
        }

        public void setAbility(AbilityBean ability) {
            this.ability = ability;
        }

        public static class AbilityBean {
            /**
             * url : http://223.99.141.141:10110/sdiot/bossopen/batchStopOpenSubs
             * params : {"appkey":""}
             * methed : POST
             * mediatype : application/json
             * isEc : false
             * tokenClass : com.yuanli.card.manage.service.QingdaoChannel
             */

            private String url;
            private Map<String, Object> params;
            private String method;
            private String mediaType;
            private String isEc;
            private String tokenClass;

            public AbstractAbility instance() {
                try {
                    Object instance = Class.forName(this.tokenClass).newInstance();
                    return (AbstractAbility) instance;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
                return null;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public Map<String, Object> getParams() {
                return params;
            }

            public AbilityBean setParams(Map<String, Object> params) {
                this.params = params;
                return this;
            }

            public String getMethod() {
                return method;
            }

            public void setMethod(String method) {
                this.method = method;
            }

            public String getMediaType() {
                return mediaType;
            }

            public AbilityBean setMediaType(String mediaType) {
                this.mediaType = mediaType;
                return this;
            }

            public String getIsEc() {
                return isEc;
            }

            public void setIsEc(String isEc) {
                this.isEc = isEc;
            }

            public String getTokenClass() {
                return tokenClass;
            }

            public void setTokenClass(String tokenClass) {
                this.tokenClass = tokenClass;
            }

        }
    }
}
