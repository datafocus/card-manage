package com.yuanli.card.manage.controller;

import com.yuanli.card.manage.service.ChannelInterface;
import com.yuanli.card.utils.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.yuanli.card.manage.service.ChannelInterfaceHelper.getAbility;

/**
 * Created by layne on 2019/1/10.
 */
@RestController
@RequestMapping("/test")
public class TestController {

    private final static Logger logger = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/t1")
    public void test() {
        ChannelInterface.AbilitiesBean.AbilityBean batchStopOpen = getAbility("ShenZhenChannel",
                "batchStopOpen");
        List<String> iccids = new ArrayList<>();
        iccids.add("89860404191730783340");
        Map<String, Object> map = new HashMap<>();
        map.put("iccids", iccids);
        map.put("optType", "1");
        //Map<String, CardFlow> cardFlowMap = batchQueryRealtimeGprsInfo.instance().batchQueryRealtimeGprsInfo(batchQueryRealtimeGprsInfo, map);
        //Map<String, CardPeriodEnum> cardPeriodEnumMap = batchQueryRealtimeGprsInfo.instance().batchQueryCardLifeCycle(batchQueryRealtimeGprsInfo, map);
        //List<String> open = batchStopOpen.instance().batchStopOpen(batchStopOpen, map);
        logger.info(JsonMapper.toJsonString(""));
    }
}
