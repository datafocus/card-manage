package com.yuanli.card.manage.service.impl;

import com.yuanli.card.manage.service.PoolFlowService;
import com.yuanli.card.mapper.PoolFlowMapper;
import com.yuanli.card.pojo.PoolFlow;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PoolFlowServiceImpl implements PoolFlowService {

    @Resource
    private PoolFlowMapper poolFlowMapper;

    @Override
    public List<PoolFlow> getByPoolCode(String poolCode) {
        return poolFlowMapper.getByPoolCode(poolCode);
    }
}
