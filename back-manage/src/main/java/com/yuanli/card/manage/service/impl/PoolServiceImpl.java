package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.PoolService;
import com.yuanli.card.mapper.PoolMapper;
import com.yuanli.card.pojo.Pool;
import com.yuanli.card.req.PoolQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PoolServiceImpl implements PoolService {

    @Resource
    private PoolMapper poolMapper;

    @Override
    public PageInfo<Pool> query(PoolQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(),"create_time desc");
        return poolMapper.query(req).toPageInfo();
    }

    @Override
    public List<Pool> list(List<Integer> customerIds) {
        return poolMapper.list(customerIds);
    }

    @Override
    public void updatePoolName(Pool pool) {
        poolMapper.updatePoolName(pool);
    }

    @Override
    public Pool getByPoolCode(String poolCode) {
        return poolMapper.getByPoolCode(poolCode);
    }
}
