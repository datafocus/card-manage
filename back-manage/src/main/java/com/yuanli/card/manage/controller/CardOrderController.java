package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.authentication.service.ISysUserService;
import com.yuanli.card.manage.service.CardOrderService;
import com.yuanli.card.manage.service.ChannelService;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.pojo.CardOrder;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.req.CardOrderQueryReq;
import com.yuanli.card.res.ResponseBean;
import com.yuanli.card.utils.JsonMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cardOrder")
public class CardOrderController {

    @Resource
    private CardOrderService cardOrderService;
    @Resource
    private ISysUserService sysUserService;
    @Resource
    private ComboService comboService;
    @Resource
    private ChannelService channelService;

    @GetMapping("/query")
    public ResponseBean query(CardOrderQueryReq req) {
        Map<String, Object> data = new HashMap<>();
        req.setCustomerId(sysUserService.currentUser().getId().intValue());
        PageInfo<CardOrder> pageInfo = cardOrderService.query(req);
        data.put("total", pageInfo.getTotal());
        data.put("list", this.fillCardOrder(pageInfo.getList()));
        return ResponseBean.success().setData(data);
    }

    private List<Map<String, Object>> fillCardOrder(List<CardOrder> list) {
        if (list == null) {
            return null;
        }
        return list.parallelStream().map(cardOrder -> {
            Map<String, Object> map = JsonMapper.defaultMapper().json2Map(JsonMapper.defaultMapper().toJson(cardOrder));
            SysUser user = sysUserService.findById(cardOrder.getSaleCustomerId().longValue());
            map.put("customerName",user==null?null:user.getNickname());
            Channel channel = channelService.findChannelByNo(cardOrder.getChannelCode());
            map.put("channelName",channel==null?null:channel.getName());
            Combo combo = comboService.getByComboNo(cardOrder.getComboCode());
            map.put("comboName",combo==null?null:combo.getComboName());
            return map;
        }).collect(Collectors.toList());
    }

}
