package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.CardOrderService;
import com.yuanli.card.mapper.CardOrderMapper;
import com.yuanli.card.pojo.CardOrder;
import com.yuanli.card.req.CardOrderQueryReq;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CardOrderServiceImpl implements CardOrderService {

    @Resource
    private CardOrderMapper cardOrderMapper;

    @Override
    public PageInfo<CardOrder> query(CardOrderQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(), "create_time desc");
        return cardOrderMapper.query(req).toPageInfo();
    }
}
