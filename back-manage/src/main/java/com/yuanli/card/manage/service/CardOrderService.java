package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.CardOrder;
import com.yuanli.card.req.CardOrderQueryReq;

public interface CardOrderService {

    PageInfo<CardOrder> query(CardOrderQueryReq req);
}
