package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.Card;
import com.yuanli.card.req.*;

import java.util.List;
import java.util.Map;

public interface CardService {

    Card getByIccid(String iccid);

    void enter(CardEnterReq req);

    PageInfo<Card> query(CardQueryReq req);

    PageInfo<Map<String,Object>> operatingQuery(CardOperatingQueryReq req);

    PageInfo<Card> operateQuery(CardOperateQueryReq req);

    void sale(CardSaleReq req);

    void stop(CardOperateQueryReq req);

    void open(CardOperateQueryReq req);

    void cancel(CardOperateQueryReq req);

    List<Card> export(CardOperateQueryReq req);

    List<Card> importQuery(List<Card> cardList,CardOperateExportReq req);

    void preclose(List<Card> cardList);

    void flowShare(List<Card> cardList);

    void autoOrder();
}
