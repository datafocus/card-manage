package com.yuanli.card.manage.service.impl;

import com.google.common.collect.Lists;
import com.yuanli.card.enumeration.CardPeriodEnum;
import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.manage.service.CardFreshService;
import com.yuanli.card.manage.service.ChannelInterfaceService;
import com.yuanli.card.mapper.CardFlowMapper;
import com.yuanli.card.mapper.CardMapper;
import com.yuanli.card.mapper.PoolFlowMapper;
import com.yuanli.card.mapper.PoolMapper;
import com.yuanli.card.pojo.CardFlow;
import com.yuanli.card.pojo.PoolFlow;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CardFreshServiceImpl implements CardFreshService {

    private static int taskNum=1000;
    @Resource
    private ChannelInterfaceService channelInterfaceService;
    @Resource
    private CardMapper cardMapper;
    @Resource
    private PoolMapper poolMapper;
    @Resource
    private CardFlowMapper cardFlowMapper;
    @Resource
    private PoolFlowMapper poolFlowMapper;


    /*
        同步卡生命期
     */
    @Override
    public void cardPeriodFresh(List<String> iccidList,Integer real) {
        if (iccidList == null || iccidList.isEmpty()) {
            return;
        }
        //todo 获取生命周期状态
        Map<String, CardPeriodEnum> periodEnumMap = channelInterfaceService.batchQueryCardLifeCycle(iccidList,null);
        if (periodEnumMap == null || periodEnumMap.isEmpty()) {
            throw new ServiceException("同步生命期-通道接口异常");
        }
        if (periodEnumMap.size() != iccidList.size()) {
            //todo 警告 查询数量与请求数量不一致
        }
        Map<String, List<String>> map = new HashMap<>();
        periodEnumMap.forEach((k,v)->{
            if (!map.containsKey(v.name())) {
                map.put(v.name(), new ArrayList<>());
            }
            map.get(v.name()).add(k);
        });
        map.forEach((k,v)->{
            if (v == null || v.isEmpty()) {
                return;
            }
            if (real == 1) {
                //todo 除去状态相同的卡
            }
            Lists.partition(v, taskNum).parallelStream().forEach(list->{
                cardMapper.freshPeriod(list,k,real);
            });
        });
    }

    @Override
    public void cardFlowFresh(List<String> iccidList){
        if (iccidList == null || iccidList.isEmpty()) {
            return;
        }
        //todo 获取卡月流量使用信息
        Map<String, CardFlow> flowMap = channelInterfaceService.batchQueryRealtimeGprsInfo(iccidList);
        if (flowMap == null || flowMap.isEmpty()) {
            throw new ServiceException("同步卡流量-通道接口异常");
        }
        if (flowMap.size() != iccidList.size()) {
            //todo 警告 查询数量与请求数量不一致
        }
        List<List<CardFlow>> partList = Lists.partition((List<CardFlow>) flowMap.values(), taskNum);
        partList.parallelStream().forEach(list->{
            cardFlowMapper.batchSave(list);
        });
    }

    @Override
    public void poolFlowFresh(List<String> iccidList){
        if (iccidList == null || iccidList.isEmpty()) {
            return;
        }
        //todo 获取流量池月流量使用信息
        Map<String, CardFlow> flowMap = channelInterfaceService.batchQueryGprsShare(iccidList);
        if (flowMap == null || flowMap.isEmpty()) {
            throw new ServiceException("同步流量池流量-通道接口异常");
        }
        if (flowMap.size() != iccidList.size()) {
            //todo 警告 查询数量与请求数量不一致
        }
        List<PoolFlow> list = new ArrayList<>();
        flowMap.forEach((k,v)->{
            PoolFlow poolFlow = new PoolFlow();
            poolFlow.setPoolCode(poolMapper.getCodeByPoolNo(v.getIccid()));
            poolFlow.setComboName(v.getComboName());
            poolFlow.setComboDate(v.getComboDate());
            poolFlow.setLeft(v.getLeft());
            poolFlow.setUsed(v.getUsed());
            poolFlow.setTotal(v.getTotal());
            list.add(poolFlow);
        });
        poolFlowMapper.batchSave(list);
    }
}
