package com.yuanli.card.manage.controller;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.authentication.service.ISysUserService;
import com.yuanli.card.manage.service.ChannelService;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.manage.service.EnterStoreService;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.EnterStore;
import com.yuanli.card.req.EnterStoreQueryReq;
import com.yuanli.card.res.ResponseBean;
import com.yuanli.card.utils.JsonMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/enterStore")
public class EnterStoreController {
    @Resource
    private EnterStoreService enterStoreService;
    @Resource
    private ISysUserService sysUserService;
    @Resource
    private ChannelService channelService;
    @Resource
    private ComboService comboService;

    @GetMapping("/query")
    public ResponseBean query(EnterStoreQueryReq req) {
        Map<String, Object> data = new HashMap<>();
        req.setCustomerId(sysUserService.currentUser().getId().intValue());
        PageInfo<EnterStore> pageInfo = enterStoreService.query(req);
        data.put("total", pageInfo.getTotal());
        data.put("list", this.fillEnterStore(pageInfo.getList()));
        return ResponseBean.success().setData(data);
    }

    private List<Map<String, Object>> fillEnterStore(List<EnterStore> list) {
        if (list == null) {
            return null;
        }
        return list.parallelStream().map(enterStore -> {
            Map<String, Object> map = JsonMapper.defaultMapper().json2Map(JsonMapper.defaultMapper().toJson(enterStore));
            Channel channel = channelService.findChannelByNo(enterStore.getChannelCode());
            map.put("channelName",channel==null?null:channel.getName());
            Combo combo = comboService.getByComboNo(enterStore.getComboCode());
            map.put("comboName",combo==null?null:combo.getComboName());
            return map;
        }).collect(Collectors.toList());
    }
}
