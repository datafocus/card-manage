package com.yuanli.card.manage.controller;

import com.yuanli.card.authentication.service.ISysRoleService;
import com.yuanli.card.pojo.SysRole;
import com.yuanli.card.res.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by layne on 2018/11/13.
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private ISysRoleService sysRoleService;

    @PostMapping
    public ResponseBean addSysRole(@RequestBody SysRole sysRole) {
        sysRoleService.addSysRole(sysRole);
        return ResponseBean.success();
    }

    @GetMapping("/delSysRole")
    public ResponseBean delSysRole(Long id) {
        sysRoleService.delSysRole(id);
        return ResponseBean.success();
    }
}
