package com.yuanli.card.manage.service;

import java.util.List;

public interface CardFreshService {

    void cardPeriodFresh(List<String> iccidList,Integer real);

    void cardFlowFresh(List<String> iccidList);

    void poolFlowFresh(List<String> iccidList);
}
