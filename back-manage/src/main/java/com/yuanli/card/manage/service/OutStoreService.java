package com.yuanli.card.manage.service;

import com.github.pagehelper.PageInfo;
import com.yuanli.card.pojo.OutStore;
import com.yuanli.card.req.OutStoreQueryReq;
import org.apache.ibatis.annotations.Param;

public interface OutStoreService {
    PageInfo<OutStore> query(OutStoreQueryReq req);

    void updateStatus(Integer id, Integer status);

    OutStore getByCardId( Integer cardId, Integer customerId);
}
