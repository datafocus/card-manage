package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.manage.service.OutStoreService;
import com.yuanli.card.mapper.OutStoreMapper;
import com.yuanli.card.pojo.OutStore;
import com.yuanli.card.req.OutStoreQueryReq;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OutStoreServiceImpl implements OutStoreService {

    @Resource
    private OutStoreMapper outStoreMapper;

    @Override
    public PageInfo<OutStore> query(OutStoreQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(), "create_time desc");
        return outStoreMapper.query(req).toPageInfo();
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        outStoreMapper.updateStatus(id,status);
    }

    @Override
    public OutStore getByCardId(Integer cardId, Integer customerId) {
        return outStoreMapper.getByCardId(cardId,customerId);
    }
}
