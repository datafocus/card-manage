package com.yuanli.card.manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuanli.card.authentication.service.ISysUserService;
import com.yuanli.card.exception.ServiceException;
import com.yuanli.card.manage.service.ComboService;
import com.yuanli.card.mapper.ComboMapper;
import com.yuanli.card.mapper.ComboRateMapper;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.req.ComboAddReq;
import com.yuanli.card.req.ComboQueryReq;
import com.yuanli.card.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ComboServiceImpl implements ComboService {
    @Autowired
    private ComboMapper comboMapper;
    @Autowired
    private ComboRateMapper comboRateMapper;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 添加套餐包
     *
     * @param req
     */
    @Override
    @Transactional
    public void save(ComboAddReq req) {
        SysUser sysUser = sysUserService.currentUser();
        if (!"ADMIN".equals(sysUser.getUser_type())) {
            throw new ServiceException(1, "您没有该操作权限");
        }
        Combo combo = new Combo();
        combo.setChannelNo(req.getChannelNo());
        combo.setComboName(req.getComboName());
        String comboNo = CommonUtil.randomCode();
        combo.setComboNo(comboNo);
        combo.setType(req.getType());
        combo.setAssistType(req.getAssistType());
        combo.setSize(req.getSizeType() == 1?req.getSize():req.getSize() * 1024);
        combo.setCreateTime(new Date());
        combo.setUpdateTime(new Date());
        combo.setChannelComboId(req.getChannelComboId());
        comboMapper.save(combo);

        ComboRate comboRate = new ComboRate();
        comboRate.setComboNo(comboNo);
        comboRate.setCustomerId(sysUser.getId().intValue());
        comboRate.setSalePrice(req.getCost()); //套餐在平台的成本价
        comboRate.setCreateTime(new Date());
        comboRate.setUpdateTime(new Date());
        comboRateMapper.save(comboRate); //为平台保存费率
    }


    /**
     * 查询套餐 只查询费率列表存在的
     *
     * @param req
     * @return
     */
    @Override
    public PageInfo query(ComboQueryReq req) {
        PageHelper.startPage(req.getPage(), req.getSize(),"create_time desc");
        Page<Combo> page = comboMapper.query(req);
        return page.toPageInfo();
    }

    /*
        查询所有套餐 不含叠加包
     */
    @Override
    public List<Combo> listAll() {
        return comboMapper.listAll();
    }

    /*
        查询客户下套餐 不含叠加包
     */
    @Override
    public List<Map<String,Object>> listByCustomer(Integer customerId) {
        return comboMapper.listByCustomer(customerId);
    }

    @Override
    public Combo getByComboNo(String comboNo) {
        return comboMapper.getByComboNo(comboNo);
    }


}
