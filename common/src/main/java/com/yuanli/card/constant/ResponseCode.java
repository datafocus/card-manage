package com.yuanli.card.constant;

/**
 * Created by layne on 2018/5/21.
 * 0:为成功
 * 负数:错误代码
 * 正数:提示语
 * -1000 - -1020 JSON错误
 * -1021 - -1040 EXCEL错误
 * -1041 - -1060 账户错误
 * -1061 - -1080 微信响应错误
 */
public enum ResponseCode {

    SYSTEM_ERROR("系统错误", -1),
    JSON_SERIALIZE_ERROR("JSON序列化出错", -1000),
    JSON_DESERIALIZE_ERROR("JSON反序列化出错", -1001),
    EXCEL_PARSE_ERROR("JSON反序列化出错", -1002),
    ACCOUNT_LOCKED("账户锁定", -1003),
    ACCOUNT_NOT_EXSIT_ERROR("账号不存在", -1004),
    PASSWORD_NOT_CORRECT("账号或者密码不正确", -1005),
    USER_EXSIT_ERROR("账号已经存在", -1006),
    PASSWORD_ENCRYPT_ERROR("加密失败", -1007),
    NOT_ALLOWED_EMPTY_PARAM("不允许空参数", -1008);


    ResponseCode(String des, Integer code) {
        this.des = des;
        this.code = code;
    }

    private String des;
    private Integer code;

    public String des() {
        return des;
    }

    public ResponseCode setDes(String des) {
        this.des = des;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public ResponseCode setCode(Integer code) {
        this.code = code;
        return this;
    }
}
