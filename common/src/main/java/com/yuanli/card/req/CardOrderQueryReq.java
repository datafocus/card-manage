package com.yuanli.card.req;

public class CardOrderQueryReq extends BaseReq{
    private String orderNo;
    private Integer saleCustomerId;
    private String channelCode;
    private String comboCode;
    private String type;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getSaleCustomerId() {
        return saleCustomerId;
    }

    public void setSaleCustomerId(Integer saleCustomerId) {
        this.saleCustomerId = saleCustomerId;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
