package com.yuanli.card.req;

import java.math.BigDecimal;

public class CustomerRechargeReq {
    private Integer rechargeCustomerId;
    private BigDecimal amount;

    public Integer getRechargeCustomerId() {
        return rechargeCustomerId;
    }

    public void setRechargeCustomerId(Integer rechargeCustomerId) {
        this.rechargeCustomerId = rechargeCustomerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
