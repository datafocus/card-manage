package com.yuanli.card.req;

public class OutStoreQueryReq extends BaseReq{
    private Integer saleCustomerId;
    private Integer status;


    public Integer getSaleCustomerId() {
        return saleCustomerId;
    }

    public void setSaleCustomerId(Integer saleCustomerId) {
        this.saleCustomerId = saleCustomerId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
