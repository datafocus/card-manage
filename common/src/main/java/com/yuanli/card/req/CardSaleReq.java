package com.yuanli.card.req;

public class CardSaleReq extends CardOperateQueryReq {
    private Integer saleCustomerId;

    public Integer getSaleCustomerId() {
        return saleCustomerId;
    }

    public void setSaleCustomerId(Integer saleCustomerId) {
        this.saleCustomerId = saleCustomerId;
    }
}
