package com.yuanli.card.req;


/**
 * Created by layne on 2018/5/24.
 * 添加新用户
 */
public class UserReq0 {
    /**
     * 用户名
     */
    public String username;
    /**
     * 密码
     */
    public String password;
    /**
     * 昵称
     */
    public String nickname;
    /**
     * 手机
     */
    public String mobile;
    /**
     * 邮箱
     */
    public String email;
    /**
     * 性别
     */
    public Integer gender;
    /**
     * 头像
     */
    public String avatar;

    /**
     * 用户类型
     */
    public String user_type;
    /**
     * 备注
     */
    public String remark;
    /**
     * 账户状态
     */
    public Integer status;

    public String getUsername() {
        return username;
    }

    public UserReq0 setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserReq0 setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public UserReq0 setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public UserReq0 setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserReq0 setEmail(String email) {
        this.email = email;
        return this;
    }

    public Integer getGender() {
        return gender;
    }

    public UserReq0 setGender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public UserReq0 setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public String getUser_type() {
        return user_type;
    }

    public UserReq0 setUser_type(String user_type) {
        this.user_type = user_type;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public UserReq0 setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public UserReq0 setStatus(Integer status) {
        this.status = status;
        return this;
    }
}
