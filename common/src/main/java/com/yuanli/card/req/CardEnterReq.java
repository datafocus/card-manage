package com.yuanli.card.req;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class CardEnterReq extends BaseReq{
    @NotNull
    private MultipartFile file;
    @NotNull
    private String channelCode;
    @NotNull
    private String comboCode;
    @NotNull
    private Integer share; //1=共享 0=不共享

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }

    public Integer getShare() {
        return share;
    }

    public void setShare(Integer share) {
        this.share = share;
    }
}
