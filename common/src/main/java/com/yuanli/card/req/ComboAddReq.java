package com.yuanli.card.req;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ComboAddReq {
    @NotNull
    private Integer size;//流量大小
    @NotNull
    private Integer sizeType;//大小单位 1=M 2=G
    @NotNull
    private String channelNo;  //所属通道编码
    @NotNull
    private String comboName;//套餐名
    @NotNull
    private String type;// 套餐类型package assist
    private String assistType;// 套餐类型month season halfyear year
    @NotNull
    private String channelComboId; //通道套餐id
    @NotNull
    private BigDecimal cost;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSizeType() {
        return sizeType;
    }

    public void setSizeType(Integer sizeType) {
        this.sizeType = sizeType;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getAssistType() {
        return assistType;
    }

    public void setAssistType(String assistType) {
        this.assistType = assistType;
    }

    public String getChannelComboId() {
        return channelComboId;
    }

    public void setChannelComboId(String channelComboId) {
        this.channelComboId = channelComboId;
    }
}
