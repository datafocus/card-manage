package com.yuanli.card.req;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ComboRateAddReq {
    @NotNull
    private String comboNo;
    @NotNull
    private Integer customerId;
    @NotNull
    private BigDecimal salePrice;

    public String getComboNo() {
        return comboNo;
    }

    public void setComboNo(String comboNo) {
        this.comboNo = comboNo;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }
}
