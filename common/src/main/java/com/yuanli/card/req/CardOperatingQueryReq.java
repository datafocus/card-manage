package com.yuanli.card.req;

import javax.validation.constraints.NotNull;

public class CardOperatingQueryReq extends BaseReq{
    private Integer status; // =0异常 =1操作中
    private Integer type; //停机 复机 取消测试期 销户
    @NotNull
    private Integer all; //=0所有 =1当前用户

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAll() {
        return all;
    }

    public void setAll(Integer all) {
        this.all = all;
    }
}
