package com.yuanli.card.req;

import com.yuanli.card.utils.DateUtils;

import java.util.Date;
import java.util.List;

public class BaseReq {
    private Integer page;
    private Integer size;
    private Date beginTime;
    private Date endTime;
    private Integer customerId; //当前用户id

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.getEndTime(endTime); //获取当天最后时间
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
}
