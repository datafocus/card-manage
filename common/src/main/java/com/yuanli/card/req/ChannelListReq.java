package com.yuanli.card.req;

import java.util.Date;

/**
 * Created by layne on 2018/11/16.
 */
public class ChannelListReq {
    //通道名称
    private String name;

    //通道编号
    private Integer status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
