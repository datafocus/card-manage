package com.yuanli.card.req;

public class CardLogQueryReq extends BaseReq{
    private Integer operateCustomerId;
    private String type; //卡操作类型 停机 复机 取消测试期 续期 变更套餐
    private Integer cardId;
    private String iccid;
    private Integer status;

    public Integer getOperateCustomerId() {
        return operateCustomerId;
    }

    public void setOperateCustomerId(Integer operateCustomerId) {
        this.operateCustomerId = operateCustomerId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
