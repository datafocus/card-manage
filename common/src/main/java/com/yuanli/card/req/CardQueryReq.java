package com.yuanli.card.req;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public class CardQueryReq extends BaseReq{
    private String iccid;
    private String channelCode;
    private String poolCode;
    private String comboCode;
    private String period; //生命期
    private Integer closeStatus;//是否主动停机
    private Integer cardCustomerId; //客户

    private List<Integer> cardCustomerIds; //卡用户id集合
    private Integer beginId;
    private Integer endId;

    private Integer valid;
    @NotNull
    private Integer saleStatus;//0=库存 1=售出

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getPoolCode() {
        return poolCode;
    }

    public void setPoolCode(String poolCode) {
        this.poolCode = poolCode;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Integer getCloseStatus() {
        return closeStatus;
    }

    public void setCloseStatus(Integer closeStatus) {
        this.closeStatus = closeStatus;
    }

    public Integer getCardCustomerId() {
        return cardCustomerId;
    }

    public void setCardCustomerId(Integer cardCustomerId) {
        this.cardCustomerId = cardCustomerId;
    }

    public List<Integer> getCardCustomerIds() {
        return cardCustomerIds;
    }

    public void setCardCustomerIds(List<Integer> cardCustomerIds) {
        this.cardCustomerIds = cardCustomerIds;
    }

    public Integer getBeginId() {
        return beginId;
    }

    public void setBeginId(Integer beginId) {
        this.beginId = beginId;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    public Integer getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(Integer saleStatus) {
        this.saleStatus = saleStatus;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }
}
