package com.yuanli.card.req;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class CardOperateExportReq extends BaseReq{
    @NotNull
    private MultipartFile file;
    private String comboCode;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }
}
