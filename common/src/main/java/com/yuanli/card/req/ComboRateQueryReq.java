package com.yuanli.card.req;

import java.util.List;

public class ComboRateQueryReq extends BaseReq{
    private String comboNo;
    private Integer rateCustomerId; //未选择则填充rateCustomerIds

    private List<Integer> rateCustomerIds;

    public String getComboNo() {
        return comboNo;
    }

    public void setComboNo(String comboNo) {
        this.comboNo = comboNo;
    }

    public Integer getRateCustomerId() {
        return rateCustomerId;
    }

    public void setRateCustomerId(Integer rateCustomerId) {
        this.rateCustomerId = rateCustomerId;
    }

    public List<Integer> getRateCustomerIds() {
        return rateCustomerIds;
    }

    public void setRateCustomerIds(List<Integer> rateCustomerIds) {
        this.rateCustomerIds = rateCustomerIds;
    }
}
