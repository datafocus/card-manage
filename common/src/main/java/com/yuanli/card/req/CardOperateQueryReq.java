package com.yuanli.card.req;

import com.yuanli.card.enumeration.CardOperateTypeEnum;

import javax.validation.constraints.NotNull;

public class CardOperateQueryReq extends BaseReq{
    private Integer beginId;
    private Integer endId;
    @NotNull
    private String comboCode; //只有取消测试期不用选择 该数据从后台获取接口
    @NotNull
    private CardOperateTypeEnum cardOperateType;

    public Integer getBeginId() {
        return beginId;
    }

    public void setBeginId(Integer beginId) {
        this.beginId = beginId;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }

    public CardOperateTypeEnum getCardOperateType() {
        return cardOperateType;
    }

    public void setCardOperateType(CardOperateTypeEnum cardOperateType) {
        this.cardOperateType = cardOperateType;
    }
}
