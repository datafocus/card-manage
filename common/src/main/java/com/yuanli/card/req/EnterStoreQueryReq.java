package com.yuanli.card.req;

public class EnterStoreQueryReq extends BaseReq{
    private String channelCode;
    private String comboCode;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }
}
