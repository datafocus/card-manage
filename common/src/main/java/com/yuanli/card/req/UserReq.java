package com.yuanli.card.req;

/**
 * Created by layne on 2018/5/24.
 * 登录用
 */
public class UserReq {
    public String username;
    public String password;
    public boolean rememberMe;

    public UserReq setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserReq setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserReq setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
        return this;
    }
}
