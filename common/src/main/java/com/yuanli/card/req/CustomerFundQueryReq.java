package com.yuanli.card.req;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CustomerFundQueryReq extends BaseReq{
    @NotNull
    private Integer queryType; //=1平台流水 =2客户流水

    private Integer type; //=1充值 =2扣款
    private Integer fundCustomerId;

    private List<Integer> customerIds;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getFundCustomerId() {
        return fundCustomerId;
    }

    public void setFundCustomerId(Integer fundCustomerId) {
        this.fundCustomerId = fundCustomerId;
    }

    public Integer getQueryType() {
        return queryType;
    }

    public void setQueryType(Integer queryType) {
        this.queryType = queryType;
    }

    public List<Integer> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<Integer> customerIds) {
        this.customerIds = customerIds;
    }
}
