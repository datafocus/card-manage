package com.yuanli.card.pojo;

import java.io.Serializable;
import java.util.Date;

public class Card implements Serializable{
    private Integer id;
    private String iccid;
    private String channelCode;
    private String poolCode;
    private String comboCode;
    private Integer customerId;
    private Integer cardLogId;
    private String period; //生命期 测试期 沉默期 正式期 其他
    private String realPeriod; //生命期 测试期 沉默期 正式期 其他
    private Integer closeStatus;// 是否主动停机 1=主动停机 0=未主动停机

    private Date createTime; //开户时间
    private Date activeTime; //激活时间
    private Date closeTime; //停机时间
    private Date openTime; //复机时间
    private Date enterTime; //客户卡入库时间


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getPoolCode() {
        return poolCode;
    }

    public void setPoolCode(String poolCode) {
        this.poolCode = poolCode;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCardLogId() {
        return cardLogId;
    }

    public void setCardLogId(Integer cardLogId) {
        this.cardLogId = cardLogId;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRealPeriod() {
        return realPeriod;
    }

    public void setRealPeriod(String realPeriod) {
        this.realPeriod = realPeriod;
    }

    public Integer getCloseStatus() {
        return closeStatus;
    }

    public void setCloseStatus(Integer closeStatus) {
        this.closeStatus = closeStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Date activeTime) {
        this.activeTime = activeTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(Date enterTime) {
        this.enterTime = enterTime;
    }
}
