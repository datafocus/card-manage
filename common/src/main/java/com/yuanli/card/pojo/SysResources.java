package com.yuanli.card.pojo;

import java.util.List;

/**
 * Created by layne on 2018/5/23.
 */
public class SysResources extends AbstractPOJO {
    private static final long serialVersionUID = -2351089482138792295L;
    private String name;
    private String type;
    private String url;
    private String action_url;
    private String permission;
    private Long parentId;
    private Integer sort;
    /**
     * 是否外部链接
     */
    private Boolean external;
    private Boolean available;
    private String icon;
    private String checked;
    private SysResources parent;
    private List<SysResources> nodes;

    public String getName() {
        return name;
    }

    public SysResources setName(String name) {
        this.name = name;
        return this;
    }

    public String getType() {
        return type;
    }

    public SysResources setType(String type) {
        this.type = type;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public SysResources setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getAction_url() {
        return action_url;
    }

    public SysResources setAction_url(String action_url) {
        this.action_url = action_url;
        return this;
    }

    public String getPermission() {
        return permission;
    }

    public SysResources setPermission(String permission) {
        this.permission = permission;
        return this;
    }

    public Long getParentId() {
        return parentId;
    }

    public SysResources setParentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }

    public Integer getSort() {
        return sort;
    }

    public SysResources setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    public Boolean getExternal() {
        return external;
    }

    public SysResources setExternal(Boolean external) {
        this.external = external;
        return this;
    }

    public Boolean getAvailable() {
        return available;
    }

    public SysResources setAvailable(Boolean available) {
        this.available = available;
        return this;
    }

    public String getIcon() {
        return icon;
    }

    public SysResources setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public String getChecked() {
        return checked;
    }

    public SysResources setChecked(String checked) {
        this.checked = checked;
        return this;
    }

    public SysResources getParent() {
        return parent;
    }

    public SysResources setParent(SysResources parent) {
        this.parent = parent;
        return this;
    }

    public List<SysResources> getNodes() {
        return nodes;
    }

    public SysResources setNodes(List<SysResources> nodes) {
        this.nodes = nodes;
        return this;
    }


}
