package com.yuanli.card.pojo;

import java.util.Date;

/**
 * Created by layne on 2018/5/23.
 * 系统用户
 */
public class SysUser extends AbstractPOJO {
    private static final long serialVersionUID = -9201224918867938901L;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 性别
     */
    private Integer gender;

    /**
     * 用户类型
     */
    private String user_type;

    /**
     * 最后登录时间
     */
    private Date last_login_time;
    /**
     * 登录次数
     */
    private Integer login_count;
    /**
     * 密码盐
     */
    private String salt;
    /**
     * 备注
     */
    private String remark;
    /*
       收货地址
     */
    private String address;
    /**
     * 账户状态
     */
    private Integer status;

    /**
     * 上级账号ID
     */
    private Long parent_id;

    public Long getParent_id() {
        return parent_id;
    }

    public SysUser setParent_id(Long parent_id) {
        this.parent_id = parent_id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public SysUser setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public SysUser setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public SysUser setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public SysUser setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public SysUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public Integer getGender() {
        return gender;
    }

    public SysUser setGender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public String getUser_type() {
        return user_type;
    }

    public SysUser setUser_type(String user_type) {
        this.user_type = user_type;
        return this;
    }

    public Date getLast_login_time() {
        return last_login_time;
    }

    public SysUser setLast_login_time(Date last_login_time) {
        this.last_login_time = last_login_time;
        return this;
    }

    public Integer getLogin_count() {
        return login_count;
    }

    public SysUser setLogin_count(Integer login_count) {
        this.login_count = login_count;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public SysUser setRemark(String remark) {
        this.remark = remark;
        return this;
    }


    public String getSalt() {
        return salt;
    }

    public SysUser setSalt(String salt) {
        this.salt = salt;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public SysUser setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
