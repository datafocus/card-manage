package com.yuanli.card.pojo;

import java.io.Serializable;
import java.util.Date;

public class CardFlow implements Serializable {
    private Long id;
    private String iccid;
    private String comboName; //套餐名
    private Double used; //已使用
    private Double left; //剩余
    private Double total; //总流量
    private Date comboDate;

    public Long getId() {
        return id;
    }

    public CardFlow setId(Long id) {
        this.id = id;
        return this;
    }

    public String getIccid() {
        return iccid;
    }

    public CardFlow setIccid(String iccid) {
        this.iccid = iccid;
        return this;
    }

    public String getComboName() {
        return comboName;
    }

    public CardFlow setComboName(String comboName) {
        this.comboName = comboName;
        return this;
    }

    public Double getUsed() {
        return used;
    }

    public CardFlow setUsed(Double used) {
        this.used = used;
        return this;
    }

    public Double getLeft() {
        return left;
    }

    public CardFlow setLeft(Double left) {
        this.left = left;
        return this;
    }

    public Double getTotal() {
        return total;
    }

    public CardFlow setTotal(Double total) {
        this.total = total;
        return this;
    }

    public Date getComboDate() {
        return comboDate;
    }

    public CardFlow setComboDate(Date comboDate) {
        this.comboDate = comboDate;
        return this;
    }
}
