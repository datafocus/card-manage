package com.yuanli.card.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CardOrder implements Serializable {
    private Integer id;
    private String orderNo;
    private Integer customerId;
    private Integer saleCustomerId;
    private String channelCode;
    private String comboCode;
    private BigDecimal comboCost;
    private BigDecimal comboPrice;
    private Integer cardNum;
    private BigDecimal amount;
    private BigDecimal profit;
    private String type;
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getSaleCustomerId() {
        return saleCustomerId;
    }

    public void setSaleCustomerId(Integer saleCustomerId) {
        this.saleCustomerId = saleCustomerId;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getComboCode() {
        return comboCode;
    }

    public void setComboCode(String comboCode) {
        this.comboCode = comboCode;
    }

    public BigDecimal getComboCost() {
        return comboCost;
    }

    public void setComboCost(BigDecimal comboCost) {
        this.comboCost = comboCost;
    }

    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    public Integer getCardNum() {
        return cardNum;
    }

    public void setCardNum(Integer cardNum) {
        this.cardNum = cardNum;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
