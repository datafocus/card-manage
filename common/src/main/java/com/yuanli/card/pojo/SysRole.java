package com.yuanli.card.pojo;

/**
 * Created by layne on 2018/5/23.
 */
public class SysRole extends AbstractPOJO {
    private static final long serialVersionUID = -2392343981091856960L;
    private String name;
    private String description;
    private Boolean available;
    private Integer selected;

    public String getName() {
        return name;
    }

    public SysRole setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SysRole setDescription(String description) {
        this.description = description;
        return this;
    }

    public Boolean getAvailable() {
        return available;
    }

    public SysRole setAvailable(Boolean available) {
        this.available = available;
        return this;
    }

    public Integer getSelected() {
        return selected;
    }

    public SysRole setSelected(Integer selected) {
        this.selected = selected;
        return this;
    }
}
