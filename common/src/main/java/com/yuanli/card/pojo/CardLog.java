package com.yuanli.card.pojo;

import java.io.Serializable;
import java.util.Date;

public class CardLog implements Serializable {
    private Integer id;
    private Integer beginId;
    private Integer endId;
    private Integer operateCustomerId;
    private String type; //卡操作类型 停机 复机 取消测试期 续期 变更套餐 停机
    private Date createTime;
    private Integer status;//0=完成 1=操作中

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBeginId() {
        return beginId;
    }

    public void setBeginId(Integer beginId) {
        this.beginId = beginId;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    public Integer getOperateCustomerId() {
        return operateCustomerId;
    }

    public void setOperateCustomerId(Integer operateCustomerId) {
        this.operateCustomerId = operateCustomerId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
