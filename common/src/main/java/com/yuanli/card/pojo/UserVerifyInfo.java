package com.yuanli.card.pojo;

/**
 * Created by layne on 2018/11/22.
 * 实名信息表
 */
public class UserVerifyInfo extends AbstractPOJO {
    private static final long serialVersionUID = -1612081493293342538L;
    //法人姓名
    private String name;
    //法人身份证号
    private String id_card;
    //公司名字
    private String company_name;
    //法人身份证正面
    private String front;
    //法人身份证反面
    private String back;
    //营业执照
    private String permit;
    //认证状态
    private Integer status;
    //客户ID
    private Long user_id;


    public String getName() {
        return name;
    }

    public UserVerifyInfo setName(String name) {
        this.name = name;
        return this;
    }

    public String getId_card() {
        return id_card;
    }

    public UserVerifyInfo setId_card(String id_card) {
        this.id_card = id_card;
        return this;
    }

    public String getCompany_name() {
        return company_name;
    }

    public UserVerifyInfo setCompany_name(String company_name) {
        this.company_name = company_name;
        return this;
    }

    public Long getUser_id() {
        return user_id;
    }

    public UserVerifyInfo setUser_id(Long user_id) {
        this.user_id = user_id;
        return this;
    }

    public String getFront() {
        return front;
    }

    public UserVerifyInfo setFront(String front) {
        this.front = front;
        return this;
    }

    public String getBack() {
        return back;
    }

    public UserVerifyInfo setBack(String back) {
        this.back = back;
        return this;
    }

    public String getPermit() {
        return permit;
    }

    public UserVerifyInfo setPermit(String permit) {
        this.permit = permit;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public UserVerifyInfo setStatus(Integer status) {
        this.status = status;
        return this;
    }
}
