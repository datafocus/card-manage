package com.yuanli.card.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by layne on 2018/5/23.
 * 此类的方法setter 方法必须放在最后调用才能实现链式调用
 */
public abstract class AbstractPOJO implements Serializable {

    private Long id;
    /**
     * 创建时间
     */
    private Date create_time;
    /**
     * 更新时间
     */
    private Date update_time;

    public Long getId() {
        return id;
    }

    public AbstractPOJO setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public AbstractPOJO setCreate_time(Date create_time) {
        this.create_time = create_time;
        return this;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public AbstractPOJO setUpdate_time(Date update_time) {
        this.update_time = update_time;
        return this;
    }
}
