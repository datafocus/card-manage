package com.yuanli.card.pojo;


import java.util.Date;

/**
 * Created by layne on 2018/11/26.
 */
public class Combo {
    private static final long serialVersionUID = 2772910418582080118L;
    private Integer id;
    //套餐大小 单位M
    private Integer size;
    //套餐编号
    private String comboNo;
    //所属通道编码
    private String channelNo;
    //套餐名
    private String comboName;
    //套餐类型
    private String type;//套餐 package和 叠加包 assist
    private String assistType;//叠加包类型
    private Date createTime;
    private Date updateTime;

    private String channelComboId;//通道产品id

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getComboNo() {
        return comboNo;
    }

    public void setComboNo(String comboNo) {
        this.comboNo = comboNo;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAssistType() {
        return assistType;
    }

    public void setAssistType(String assistType) {
        this.assistType = assistType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getChannelComboId() {
        return channelComboId;
    }

    public void setChannelComboId(String channelComboId) {
        this.channelComboId = channelComboId;
    }
}
