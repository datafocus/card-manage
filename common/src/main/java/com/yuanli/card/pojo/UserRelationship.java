package com.yuanli.card.pojo;

/**
 * Created by layne on 2018/11/21.
 * 客户关系表
 */
public class UserRelationship extends AbstractPOJO {

    private static final long serialVersionUID = -1972960630775306465L;

    private Long parent;
    private Long child;

    public Long getParent() {
        return parent;
    }

    public UserRelationship setParent(Long parent) {
        this.parent = parent;
        return this;
    }

    public Long getChild() {
        return child;
    }

    public UserRelationship setChild(Long child) {
        this.child = child;
        return this;
    }
}
