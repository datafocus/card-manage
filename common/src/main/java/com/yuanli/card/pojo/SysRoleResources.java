package com.yuanli.card.pojo;

/**
 * Created by layne on 2018/5/23.
 */
public class SysRoleResources extends AbstractPOJO {
    private static final long serialVersionUID = 3214301384754842151L;
    private Long roleId;
    private Long resourcesId;

    public Long getRoleId() {
        return roleId;
    }

    public SysRoleResources setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }

    public Long getResourcesId() {
        return resourcesId;
    }

    public SysRoleResources setResourcesId(Long resourcesId) {
        this.resourcesId = resourcesId;
        return this;
    }
}
