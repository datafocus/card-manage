package com.yuanli.card.pojo;

/**
 * Created by layne on 2018/5/23.
 */
public class SysUserRole extends AbstractPOJO {
    private static final long serialVersionUID = -9143641728830796818L;
    private Long userId;
    private Long roleId;

    public Long getUserId() {
        return userId;
    }

    public SysUserRole setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Long getRoleId() {
        return roleId;
    }

    public SysUserRole setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }
}
