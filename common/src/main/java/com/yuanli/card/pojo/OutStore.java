package com.yuanli.card.pojo;

import java.io.Serializable;
import java.util.Date;

public class OutStore implements Serializable {
    private Integer id;
    private Integer cardNum;
    private Integer beginId;
    private Integer endId;
    private Integer customerId;
    private Integer saleCustomerId;
    private String address;
    private Integer status; //发货状态 1=已发货 0=未发货
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCardNum() {
        return cardNum;
    }

    public void setCardNum(Integer cardNum) {
        this.cardNum = cardNum;
    }

    public Integer getBeginId() {
        return beginId;
    }

    public void setBeginId(Integer beginId) {
        this.beginId = beginId;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getSaleCustomerId() {
        return saleCustomerId;
    }

    public void setSaleCustomerId(Integer saleCustomerId) {
        this.saleCustomerId = saleCustomerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
