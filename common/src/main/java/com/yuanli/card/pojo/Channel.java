package com.yuanli.card.pojo;

import java.math.BigDecimal;

/**
 * Created by layne on 2018/11/15.
 */
public class Channel extends AbstractPOJO {
    private static final long serialVersionUID = -7023278733063049975L;
    //通道名称
    private String name;

    //运营商
    private String operator;

    //通道编号
    private String channle_no;

    //通道余额
    private BigDecimal balance;

    //通道状态
    private Integer status;

    private String channel_class_name;

    public String getChannel_class_name() {
        return channel_class_name;
    }

    public Channel setChannel_class_name(String channel_class_name) {
        this.channel_class_name = channel_class_name;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public Channel setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Channel setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public String getName() {
        return name;
    }

    public Channel setName(String name) {
        this.name = name;
        return this;
    }

    public String getChannle_no() {
        return channle_no;
    }

    public Channel setChannle_no(String channle_no) {
        this.channle_no = channle_no;
        return this;
    }


    public String getOperator() {
        return operator;
    }

    public Channel setOperator(String operator) {
        this.operator = operator;
        return this;
    }
}
