package com.yuanli.card.pojo;

import java.io.Serializable;
import java.util.Date;

public class PoolFlow implements Serializable {
    private Integer id;
    private String poolCode;
    private String comboName;
    private Double used;
    private Double left;
    private Double total;
    private Date comboDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPoolCode() {
        return poolCode;
    }

    public void setPoolCode(String poolCode) {
        this.poolCode = poolCode;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }

    public Double getUsed() {
        return used;
    }

    public PoolFlow setUsed(Double used) {
        this.used = used;
        return this;
    }

    public Double getLeft() {
        return left;
    }

    public PoolFlow setLeft(Double left) {
        this.left = left;
        return this;
    }

    public Double getTotal() {
        return total;
    }

    public PoolFlow setTotal(Double total) {
        this.total = total;
        return this;
    }

    public Date getComboDate() {
        return comboDate;
    }

    public void setComboDate(Date comboDate) {
        this.comboDate = comboDate;
    }
}
