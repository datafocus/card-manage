package com.yuanli.card.enumeration;

/**
 * Created by layne on 2018/11/23.
 */
public enum OperatorEnum {
    CMCC(1, "中国移动"), CTCC(2, "中国电信"), CUCC(3, "中国联通");

    //编号
    private Integer no;
    //运营商名称
    private String name;

    OperatorEnum(int no, String name) {
        this.name = name;
        this.no = no;
    }
}
