package com.yuanli.card.enumeration;

public enum CardSearchTypeEnum {
    gprs,
    status,
    period,
    flow,
    pool_flow,
    month_flow,
    month_pool_flow
}
