package com.yuanli.card.enumeration;

public enum CardPeriodEnum {
    test("测试期"),
    silent("沉默期"),
    normal("正使用"),
    inventory("库存期"),
    stop("停机"),
    preclose("销户"),
    bespeakClose("预约销户"),
    other("其他");

    private String desc;

    CardPeriodEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
