package com.yuanli.card.enumeration;

public enum CardLogTypeEnum {
    sale("出售"),
    stop("停机"),
    open("开机"),
    cancel("取消测试期"),
    preclose("销户"),
    flowshare("流量共享");

    private String name;

    CardLogTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
