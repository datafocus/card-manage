package com.yuanli.card.enumeration;

public enum ComboTypeEnum  {
    month("月包"),
    season("季包"),
    halfyear("半年包"),
    year("年包");

    private String name;

    ComboTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
