package com.yuanli.card.enumeration;

public enum CardStatusEnum {
    inactive("待激活"),
    normal("正常"),
    stoped("停机"),
    invalid("销户"),
    other("其他");

    private String desc;

    CardStatusEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
