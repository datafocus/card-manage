package com.yuanli.card.enumeration;

public enum CardOperateTypeEnum {

    sale, //出售
    stop, //停机
    open, //复机
    cancel, //取消测试期
    export //导出
}
