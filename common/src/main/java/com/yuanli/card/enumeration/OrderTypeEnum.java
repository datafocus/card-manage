package com.yuanli.card.enumeration;

public enum OrderTypeEnum {
    sale, //售出扣款
    open, //复机扣款
    expand, //续期扣款
    auto //自动扣款
}
