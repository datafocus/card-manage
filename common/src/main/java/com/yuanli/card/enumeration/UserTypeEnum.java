package com.yuanli.card.enumeration;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by layne on 2018/5/23.
 */
public enum UserTypeEnum {

    ROOT("超级管理员"),
    ADMIN("管理员"),
    USER("系统会员"),
    CUSTOMER("客户"),
    UNKNOW("未知");

    private String desc;

    UserTypeEnum(String desc) {
        this.desc = desc;
    }

    public static UserTypeEnum getByType(String type) {
        if (StringUtils.isEmpty(type)) {
            return UserTypeEnum.UNKNOW;
        }
        for (UserTypeEnum ut : UserTypeEnum.values()) {
            if (ut.toString().equalsIgnoreCase(type)) {
                return ut;
            }
        }
        return UserTypeEnum.UNKNOW;
    }

    public String getDesc() {
        return desc;
    }
}
