package com.yuanli.card.res;

import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.stream.Collectors;
import static com.yuanli.card.utils.BeanUtils.*;


/**
 * Created by layne on 2018/6/8.
 */
public class PageRes<T> {
    //当前页
    private int pageNum;
    //每页的数量
    private int pageSize;
    //当前页的数量
    private int size;
    //总记录数
    private long total;
    //总页数
    private int pages;
    //结果集
    private List<T> list;
    //是否有下一页
    private boolean hasNextPage = false;

    public PageRes(PageInfo pageInfo) {
        this(pageInfo, null);
    }

    public PageRes(PageInfo pageInfo, Class<T> tagetClass) {
        this.pageNum = pageInfo.getPageNum();
        this.pageSize = pageInfo.getPageSize();
        this.size = pageInfo.getSize();
        this.total = pageInfo.getTotal();
        this.hasNextPage = pageInfo.isHasNextPage();
        if (tagetClass != null) {
            this.list = (List<T>) pageInfo.getList()
                    .parallelStream()
                    .map(o -> copyNotNullProperties(o, getInstance(tagetClass)))
                    .collect(Collectors.toList());
        } else {
            this.list = pageInfo.getList();
        }
    }

    public int getPageNum() {
        return pageNum;
    }

    public PageRes<T> setPageNum(int pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public PageRes<T> setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public int getSize() {
        return size;
    }

    public PageRes<T> setSize(int size) {
        this.size = size;
        return this;
    }

    public long getTotal() {
        return total;
    }

    public PageRes<T> setTotal(long total) {
        this.total = total;
        return this;
    }

    public int getPages() {
        return pages;
    }

    public PageRes<T> setPages(int pages) {
        this.pages = pages;
        return this;
    }

    public List<T> getList() {
        return list;
    }

    public PageRes<T> setList(List<T> list) {
        this.list = list;
        return this;
    }

    public boolean getHasNextPage() {
        return hasNextPage;
    }

    public PageRes<T> setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
        return this;
    }

}
