package com.yuanli.card.res;

import java.math.BigDecimal;

public class FundCountAmountRes {

    private BigDecimal rechargeAmount;
    private BigDecimal consumeAmount;

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getConsumeAmount() {
        return consumeAmount;
    }

    public void setConsumeAmount(BigDecimal consumeAmount) {
        this.consumeAmount = consumeAmount;
    }

}
