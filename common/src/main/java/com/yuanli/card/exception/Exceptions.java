package com.yuanli.card.exception;

/**
 * Created by layne on 2019/3/6.
 */
public class Exceptions {
    public static <T> T requireNonNull(T obj, String message) {
        if (obj == null)
            throw new ServiceException(999, message);
        return obj;
    }

    public static <T> T requireNonNull(T obj) {
        if (obj == null)
            throw new ServiceException(999, "对象不能为空");
        return obj;
    }

    public static <T> T[] requireNonNull(T... objs) {
        if (objs == null)
            throw new ServiceException(999, "对象不能为空");
        for (T obj : objs) {
            if (obj == null)
                throw new ServiceException(999, "对象不能为空");
        }
        return objs;
    }

    public static <T> T[] requireNonNull(String message, T... objs) {
        if (objs == null)
            throw new ServiceException(999, message);
        for (T obj : objs) {
            if (obj == null)
                throw new ServiceException(999, message);
        }
        return objs;
    }
}
