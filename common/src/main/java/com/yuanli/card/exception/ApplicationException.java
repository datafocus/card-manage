package com.yuanli.card.exception;

/**
 * @author layne
 * @date 17-7-26
 * 非受检异常的顶级父类
 */
public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    protected Integer code;
    protected Object[] args;
    protected Object arg;

    public ApplicationException() {
        super();
    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ApplicationException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public ApplicationException(String message, Object[] args) {
        super(message);
        this.args = args;
    }

    public ApplicationException(String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.args = args;
    }

    public ApplicationException(Integer code, String message, Object[] args) {
        super(message);
        this.code = code;
        this.args = args;
    }

    public ApplicationException(Integer code, String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.code = code;
        this.args = args;
    }

    public Integer getCode() {
        return this.code;
    }

    public Object[] getArgs() {
        return this.args;
    }
}
