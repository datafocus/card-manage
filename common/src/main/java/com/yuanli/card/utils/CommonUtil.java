package com.yuanli.card.utils;


import java.util.Date;
import java.util.Random;

public class CommonUtil {
    /*
    13位code
    8为日期 5位随机
     */
    public static String randomCode() {
        return DateUtils.format(new Date(), "yyyyMMdd")+CommonUtil.randomNumber(5);
    }


    /*
    随机数字字符串
     */
    public static String randomNumber(int num) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < num; i++) {
            result.append(new Random().nextInt(10));
        }
        return result.toString();
    }
}
