package com.yuanli.card.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by layne on 2018/6/8.
 */
public class BeanUtils {

    public static <T> T copyNotNullProperties(Object source, T taget) {
        List<String> ignoreFileds = new ArrayList<>();
        Field[] declaredFields = source.getClass().getDeclaredFields();
        for (Field declaredField : declaredFields) {
            try {
                declaredField.setAccessible(true);
                Object o = declaredField.get(source);
                if (o == null) {
                    ignoreFileds.add(declaredField.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        org.springframework.beans.BeanUtils.copyProperties(source, taget, ignoreFileds.toArray(new String[ignoreFileds.size()]));
        return taget;
    }

    public static <T> T getInstance(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
