package com.yuanli.card.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by layne on 2018/5/23.
 */
public class ExcelTileFiledIndex {
    /**
     * 列名 - 字段名 映射
     */
    private Map<String, String> columnNameFiledNameMap;

    /**
     * 列索引 - 列名 映射
     */
    private Map<String, Integer> columnNameIndexMap;

    /**
     * 索引
     */
    private Integer index = null;


    /**
     * 手动设置每列的索引值
     *
     * @param columnName 列名
     * @param fileName   字段名
     * @param index      列在excel中列索引
     * @return
     */
    public ExcelTileFiledIndex addWithIndex(String columnName, String fileName, Integer index) {
        if (columnNameFiledNameMap == null) {
            columnNameFiledNameMap = new HashMap<>();
        }
        if (columnNameIndexMap == null) {
            columnNameIndexMap = new HashMap<>();
        }
        columnNameIndexMap.put(columnName, index);
        columnNameFiledNameMap.put(columnName, fileName);
        return this;
    }

    /**
     * 按添加顺序生成列索引
     *
     * @param columnName
     * @param fileName
     * @return
     */
    public ExcelTileFiledIndex addByAutoIndex(String columnName, String fileName) {
        if (columnNameFiledNameMap == null) {
            columnNameFiledNameMap = new HashMap<>();
        }
        if (columnNameIndexMap == null) {
            columnNameIndexMap = new HashMap<>();
        }
        if (index == null) {
            index = 0;
        }
        synchronized (this.index) {
            columnNameIndexMap.put(columnName, index++);
        }
        columnNameFiledNameMap.put(columnName, fileName);
        return this;
    }

    /**
     * 用于在excel转list时
     * 不需要知道列索引的情况下
     *
     * @param columnName
     * @param fileName
     * @return
     */
    public ExcelTileFiledIndex add(String columnName, String fileName) {
        if (columnNameFiledNameMap == null) {
            columnNameFiledNameMap = new HashMap<>();
        }
        columnNameFiledNameMap.put(columnName, fileName);
        return this;
    }

    public Map<String, String> getColumnNameFiledNameMap() {
        return columnNameFiledNameMap;
    }

    public Map<String, Integer> getColumnNameIndexMap() {
        return columnNameIndexMap;
    }
}
