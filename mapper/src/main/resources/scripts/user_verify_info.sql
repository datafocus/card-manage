CREATE TABLE user_verify_info (
  `id`           INT      AUTO_INCREMENT
  COMMENT 'ID',
  `name`         VARCHAR(10) COMMENT '法人身姓名',
  `id_card`      VARCHAR(20) COMMENT '法人身份证号',
  `company_name` VARCHAR(50) COMMENT '公司名',
  `user_id`      INT COMMENT '系统用户ID',
  `front`        VARCHAR(200) COMMENT '法人身份证正面',
  `back`         VARCHAR(200) COMMENT '法人身份证反面',
  `permit`       VARCHAR(200) COMMENT '营业执照',
  `status`       INT COMMENT '状态 1:审核中,2,审核通过',
  `create_time`  DATETIME DEFAULT CURRENT_TIMESTAMP
  COMMENT '创建日期',
  `update_time`  DATETIME DEFAULT CURRENT_TIMESTAMP
  COMMENT '修改日期',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDb
  CHARSET = utf8mb4
  COLLATE = utf8mb4_bin
  AUTO_INCREMENT = 0