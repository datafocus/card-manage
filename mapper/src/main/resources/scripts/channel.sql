CREATE TABLE channel (
  `id`          INT      AUTO_INCREMENT
  COMMENT 'ID',
  `name`        VARCHAR(50) UNIQUE
  COMMENT '通道名',
  `channle_no`  VARCHAR(50) UNIQUE
  COMMENT '通道编码',
  `operator`    VARCHAR(5) COMMENT '运营商',
  `balance`     DECIMAL COMMENT '通道余额',
  `status`      INT COMMENT '通道状态 0 停用 1 启用',
  `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP
  COMMENT '创建日期',
  `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP
  COMMENT '修改日期',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDb
  CHARSET = utf8mb4
  COLLATE = utf8mb4_bin
  AUTO_INCREMENT = 0;