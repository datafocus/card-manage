CREATE TABLE `package` (
  `id`          INT      AUTO_INCREMENT
  COMMENT 'ID',
  `size`        INT COMMENT '套餐大小,单位M',
  `package_no`  VARCHAR(20) COMMENT '套餐编号',
  `channel_no`  VARCHAR(20) COMMENT '所属通道编号',
  `name`        VARCHAR(30) COMMENT '套餐名',
  `custom`      BOOL COMMENT '是否自定义套餐',
  `status`      INT COMMENT '套餐状态 0:停用,1:启用',
  `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP
  COMMENT '创建日期',
  `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP
  COMMENT '修改日期',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDb
  CHARSET = utf8mb4
  COLLATE = utf8mb4_bin
  AUTO_INCREMENT = 0;