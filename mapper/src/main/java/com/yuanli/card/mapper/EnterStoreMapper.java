package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.EnterStore;
import com.yuanli.card.req.EnterStoreQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EnterStoreMapper {
    void save(EnterStore enterStore);

    Page<EnterStore> query(EnterStoreQueryReq req);
}
