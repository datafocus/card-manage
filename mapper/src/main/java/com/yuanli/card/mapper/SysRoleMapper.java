package com.yuanli.card.mapper;

import com.yuanli.card.pojo.SysRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by layne on 2018/11/13.
 */
@Repository
public interface SysRoleMapper {
    void save(SysRole sysRole);

    void delByid(@Param("id") Long id);
}
