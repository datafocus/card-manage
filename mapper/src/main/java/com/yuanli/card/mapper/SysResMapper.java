package com.yuanli.card.mapper;

import com.yuanli.card.pojo.SysResources;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by layne on 2018/11/13.
 */
@Repository
public interface SysResMapper {
    void save(SysResources sysResources);

    void delByid(@Param("id") Long id);
}
