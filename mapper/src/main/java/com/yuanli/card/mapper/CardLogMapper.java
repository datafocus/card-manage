package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.CardLog;
import com.yuanli.card.req.CardLogQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CardLogMapper {

    void save(CardLog cardLog);

    Page<CardLog> query(CardLogQueryReq req);

    void updateStatus(@Param("id") Integer id);
}
