package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.CustomerFund;
import com.yuanli.card.req.CustomerFundQueryReq;
import com.yuanli.card.res.FundCountAmountRes;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerFundMapper {
    void save(CustomerFund customerFund);

    Page<CustomerFund> query(CustomerFundQueryReq req);

    FundCountAmountRes countAmount(CustomerFundQueryReq req);
}
