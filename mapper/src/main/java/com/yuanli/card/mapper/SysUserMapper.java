package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.enumeration.UserTypeEnum;
import com.yuanli.card.pojo.SysUser;
import com.yuanli.card.req.SysUserQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by layne on 2018/5/24.
 */
@Repository
public interface SysUserMapper {
    void save(SysUser sysUser);

    SysUser findByUsername(@Param("username") String username);

    SysUser findById(@Param("id") Long id);

    void updateUserLastLoginInfoById(@Param("id") Long id, @Param("lastLogin") Date lastLogin);

    void delById(@Param("id") Long id);

    List<Integer> findChildById(@Param("id") Integer id);

    List<SysUser> listChild(SysUserQueryReq req);

    List<SysUser> listAll();

    void update(SysUser sysUser);
}
