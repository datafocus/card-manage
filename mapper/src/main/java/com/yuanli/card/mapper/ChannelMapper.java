package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.Channel;
import com.yuanli.card.req.ChannelListReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by layne on 2018/11/15.
 */
@Repository
public interface ChannelMapper {

    void save(Channel channel);

    List<Channel> finfAllChannelNameAndNo();

    Page<Channel> findAll(ChannelListReq req);

    /**
     * 根据通道编号查询通道信息
     *
     * @param no
     * @return
     */
    Channel findChannelByNo(@Param("no") String no);

    void changeChannelStatus(@Param("status") int status,@Param("id") Long id);

    int findChannelStatusById(@Param("id") long id);
}
