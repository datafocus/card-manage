package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.req.ComboQueryReq;
import com.yuanli.card.req.ComboRateQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by layne on 2018/11/26.
 */
@Repository
public interface ComboRateMapper {
    void save(ComboRate comboRate);

    Page<ComboRate> query(ComboRateQueryReq req);

    void updateSalePrice(ComboRate comboRate);

    ComboRate getByComboAndCustomer(@Param("comboNo") String comboNo, @Param("customerId") Integer customerId);
}
