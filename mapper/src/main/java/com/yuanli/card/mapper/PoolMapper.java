package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.Pool;
import com.yuanli.card.req.PoolQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PoolMapper {
    void save(Pool pool);

    void updatePoolName(Pool pool);

    Page<Pool> query(PoolQueryReq req);

    List<Pool> list(@Param("customerIds") List<Integer> customerIds);

    void updatePoolCustomer(@Param("poolCode") String poolCode, @Param("customerId") Integer customerId);

    Pool getByPoolCode(@Param("poolCode") String poolCode);

    List<String> listAllPoolNo();

    String getCodeByPoolNo(@Param("poolNo") String poolNo);
}
