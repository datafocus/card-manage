package com.yuanli.card.mapper;

import com.yuanli.card.pojo.CardFlow;
import com.yuanli.card.pojo.PoolFlow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PoolFlowMapper {
    List<PoolFlow> getByPoolCode(@Param("poolCode") String poolCode);

    void batchSave(List<PoolFlow> flowList);
}
