package com.yuanli.card.mapper;

import com.yuanli.card.pojo.CardFlow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardFlowMapper {
    List<CardFlow> getByIccid(@Param("iccid") String iccid);

    void batchSave(List<CardFlow> flowList);
}
