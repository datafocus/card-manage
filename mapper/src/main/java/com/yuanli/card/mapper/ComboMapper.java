package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.Combo;
import com.yuanli.card.pojo.ComboRate;
import com.yuanli.card.req.ComboQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by layne on 2018/11/26.
 */
@Repository
public interface ComboMapper {
    void save(Combo combo);

    /*
        套餐列表 包含月包和叠加包
     */
    Page<Combo> query(ComboQueryReq req);

    Combo getByComboNo(@Param("comboNo") String comboNo);

    /*
        查询所有月包
     */
    List<Combo> listAll();

    /*
        查询客户月包
     */
    List<Map<String,Object>> listByCustomer(@Param("customerId") Integer customerId);
}
