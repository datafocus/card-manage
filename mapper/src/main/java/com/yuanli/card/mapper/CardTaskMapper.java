package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.CardTask;
import com.yuanli.card.req.CardTaskQueryReq;
import org.springframework.stereotype.Repository;

@Repository
public interface CardTaskMapper {
    void save(CardTask cardTask);

    Page<CardTask> query(CardTaskQueryReq req);
}
