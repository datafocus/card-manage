package com.yuanli.card.mapper;

import com.yuanli.card.pojo.UserVerifyInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by layne on 2018/11/22.
 */
@Repository
public interface UserVerifyInfoMapper {
    void save(UserVerifyInfo userVerifyInfo);

    void changeStatus(@Param("id") Long id,@Param("status") int status);
}
