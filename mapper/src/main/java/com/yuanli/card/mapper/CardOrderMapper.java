package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.CardOrder;
import com.yuanli.card.req.CardOrderQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CardOrderMapper {
    void save(CardOrder cardOrder);

    Page<CardOrder> query(CardOrderQueryReq req);
}
