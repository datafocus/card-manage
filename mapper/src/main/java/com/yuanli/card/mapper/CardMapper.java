package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.bus.CardRange;
import com.yuanli.card.bus.GroupUserCombo;
import com.yuanli.card.pojo.Card;
import com.yuanli.card.req.CardOperatingQueryReq;
import com.yuanli.card.req.CardQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface CardMapper {
    void batchSave(@Param("cardList") List<Card> cardList); //todo 设置max_allowed_packet
    Integer getMaxId();

    Page<Card> query(@Param("req") CardQueryReq req);
    Page<Map<String,Object>> operatingQuery(CardOperatingQueryReq req);
    List<Card> queryAll(@Param("req") CardQueryReq req);
    List<String> queryIccidList(@Param("req") CardQueryReq req);
    Integer count(@Param("req") CardQueryReq req);
    CardRange countCardRange(@Param("req") CardQueryReq req);

    void updateBySale(@Param("req") CardQueryReq req, @Param("customerId") Integer customerId, @Param("now")Date now); //修改卡的所属客户 入库时间
    void updateByStop(@Param("req") CardQueryReq req, @Param("cardLogId") Integer cardLogId, @Param("now") Date now);//修改卡状态 主动停机状态 停机时间
    void updateByOpen(@Param("req") CardQueryReq req, @Param("cardLogId") Integer cardLogId, @Param("now") Date now);//修改卡状态 主动停机状态 复机时间
    void updateByCancel(@Param("req") CardQueryReq req, @Param("cardLogId") Integer cardLogId);//修改卡生命期

    List<Card> importQuery(@Param("beginId") Integer beginId, @Param("endId") Integer endId);
    void updatePoolCode(@Param("beginId") Integer beginId, @Param("endId") Integer endId,@Param("poolCode") String poolCode);
    void updateByClose(@Param("beginId") Integer beginId, @Param("endId") Integer endId, @Param("cardLogId") Integer cardLogId);

    Card getById(@Param("id") Integer id);
    Card getByIccid(@Param("iccid") String iccid);

    void freshPeriod(@Param("iccidList") List<String> iccidList, @Param("period") String period, @Param("real") Integer real);

    List<String> listAllIccid();
    List<GroupUserCombo> groupUserCombo();
}
