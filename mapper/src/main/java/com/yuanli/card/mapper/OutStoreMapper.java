package com.yuanli.card.mapper;

import com.github.pagehelper.Page;
import com.yuanli.card.pojo.OutStore;
import com.yuanli.card.req.OutStoreQueryReq;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OutStoreMapper {

    void save(OutStore outStore);

    Page<OutStore> query(OutStoreQueryReq req);

    void updateStatus(@Param("id") Integer id, @Param("status") Integer status);

    OutStore getByCardId(@Param("cardId") Integer cardId, @Param("customerId") Integer customerId);
}
